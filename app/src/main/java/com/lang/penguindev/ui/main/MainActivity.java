package com.lang.penguindev.ui.main;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.lang.penguindev.R;
import com.lang.penguindev.path.ARouterPath;
import com.lang.penguindev.ui.AppBaseActivity;

/**
*@Author : DirainZhang
*@Createtime : 2020/9/15 15:15
*@Describe :
*/
@Route(path = ARouterPath.MAIN_PATH)
public class MainActivity extends AppBaseActivity {

    @Override
    public int initLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void initViews(@Nullable Bundle savedInstanceState) {

    }

    @Override
    public void loadData(@Nullable Bundle savedInstanceState) {

    }
}