package com.lang.penguindev.ui;

import com.running.penguin.base.BaseFragment;
import com.running.penguin.mvp.BasePresenter;

public abstract class AppBaseFragment extends BaseFragment {
    @Override
    protected void onFragmentFirstVisible() {

    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    public void initVariables() {

    }

}
