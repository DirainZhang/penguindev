package com.lang.penguindev.ui.splash;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.lang.penguindev.R;
import com.lang.penguindev.path.ARouterPath;
import com.lang.penguindev.ui.AppBaseActivity;
import com.running.penguin.constants.Constants;
import com.running.penguin.utils.ToastUtils;

/**
*@Author : DirainZhang
*@Createtime : 2020/9/15 15:00
*@Describe : 
*/
@Route(path = ARouterPath.SPLASH_PATH)
public class SplashActivity extends AppBaseActivity {

    protected String pathActivity = ARouterPath.MAIN_PATH;
    protected ImageView splashImg;
    protected TextView splashTextView;

    @Override
    public int initLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public void initViews(@Nullable Bundle savedInstanceState) {
        splashImg = (ImageView)findViewById(R.id.splash);
        splashTextView = (TextView)findViewById(R.id.splash_text);
    }

    @Override
    public void loadData(@Nullable Bundle savedInstanceState) {
        checkCameraPermission();
    }

    private void StartNextActivity(String pathActivity) {
        new Handler().postDelayed(() -> {
            ARouter.getInstance().build(pathActivity).navigation();
            finish();
        }, 2 * 1000);
    }

    protected void checkCameraPermission() {
        Constants.NO_VIDEO_PERMISSION.clear();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (int i = 0; i < Constants.VIDEO_PERMISSION.length; i++) {
                if (ActivityCompat.checkSelfPermission(this, Constants.VIDEO_PERMISSION[i]) != PackageManager.PERMISSION_GRANTED) {
                    Constants.NO_VIDEO_PERMISSION.add(Constants.VIDEO_PERMISSION[i]);
                }
            }
            if (Constants.NO_VIDEO_PERMISSION.size() == 0) {
                StartNextActivity(pathActivity);
            } else {
                ActivityCompat.requestPermissions(SplashActivity.this, Constants.NO_VIDEO_PERMISSION.toArray(new String[Constants.NO_VIDEO_PERMISSION.size()]), Constants.REQUEST_CAMERA);
            }
        } else {
            StartNextActivity(pathActivity);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == Constants.REQUEST_CAMERA) {
            boolean flag = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    flag = true;
                } else {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                ToastUtils.showSuccessToast("已授权!");
                StartNextActivity(pathActivity);
            } else {
                ToastUtils.showErrorToast("授权失败!");
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}