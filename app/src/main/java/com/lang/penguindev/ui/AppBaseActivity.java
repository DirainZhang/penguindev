package com.lang.penguindev.ui;

import androidx.annotation.NonNull;

import com.lang.penguindev.mvp.AppPresenter;
import com.running.penguin.base.BaseActivity;
import com.running.penguin.mvp.IView;

/**
*@Author : DirainZhang
*@Createtime : 2020/9/15 14:56
*@Describe :
*/
public abstract class AppBaseActivity extends BaseActivity<AppPresenter<IView>> implements IView {


    @Override
    protected void initToolBar() {

    }

    @Override
    public void initVariables() {

    }

    @Override
    protected AppPresenter<IView> createPresenter() {
        return null;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(@NonNull String message) {

    }

    @Override
    public void showContentState() {

    }

    @Override
    public void showEmptyState() {

    }

    @Override
    public void showErrorState() {

    }

    @Override
    public void refreshCompleted() {

    }

    @Override
    public void nonMoreData(boolean hasMore) {

    }
}
