package com.lang.penguindev.mvp;

import com.lang.penguindev.api.ApiInterface;
import com.running.penguin.http.RetrofitManager;
import com.running.penguin.mvp.BasePresenter;

/**
 * @Author : zlang-pc
 * @Createtime : 2020/7/30 20:42
 * @Describe :
 */
public class AppPresenter<IView> extends BasePresenter<IView> {

    protected ApiInterface api = null;

    public AppPresenter(IView view) {
        super(view);
        api = RetrofitManager.getHttpsApiService(ApiInterface.class);
    }

}
