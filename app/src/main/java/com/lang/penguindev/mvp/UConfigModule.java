package com.lang.penguindev.mvp;

import android.app.Application;
import android.content.Context;

import androidx.fragment.app.FragmentManager;

import com.running.penguin.base.delegate.AppLifecycles;
import com.running.penguin.integration.ConfigModule;

import java.util.List;

public class UConfigModule implements ConfigModule {

    @Override
    public void applyOptions(Context context) {

    }

    @Override
    public void injectAppLifecycle(Context context, List<AppLifecycles> lifecycles) {
        lifecycles.add(new ULifecycle());
    }

    @Override
    public void injectActivityLifecycle(Context context, List<Application.ActivityLifecycleCallbacks> lifecycles) {

    }

    @Override
    public void injectFragmentLifecycle(Context context, List<FragmentManager.FragmentLifecycleCallbacks> lifecycles) {

    }
}
