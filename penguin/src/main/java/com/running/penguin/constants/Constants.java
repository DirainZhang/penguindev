package com.running.penguin.constants;

import android.Manifest;

import java.util.ArrayList;
import java.util.List;

public class Constants {

    public static int SCREEN_WIDTH = 400;
    public static int SCREEN_HEIGHT = 800;
    //权限列表
    public static String[] VIDEO_PERMISSION = {
            Manifest.permission.CAMERA,//相机权限
            Manifest.permission.RECORD_AUDIO,//录音权限
            Manifest.permission.WRITE_EXTERNAL_STORAGE,//写入SDCard权限
            Manifest.permission.READ_EXTERNAL_STORAGE,//读取SDCard权限
//            Manifest.permission.CALL_PHONE,//打电话权限
            Manifest.permission.READ_PHONE_STATE,//读取手机状态权限
            Manifest.permission.ACCESS_COARSE_LOCATION//定位权限
    };
    public static List<String> NO_VIDEO_PERMISSION = new ArrayList<>();
    public static final int REQUEST_CAMERA = 0;

}
