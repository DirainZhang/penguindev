package com.running.penguin.constants

import android.Manifest
import java.util.ArrayList

/**
 *@Author : zlang-pc
 *@Createtime : 2020/6/3 15:32
 *@Describe :
 */
object EConstants {

    var SCREEN_WIDTH:Double = 400.0
    var SCREEN_HEIGHT:Double = 800.0

    //权限列表
    var STORAGE_PERMISSION = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE, //写入SDCard权限
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.CALL_PHONE,
//        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.CAMERA,
        Manifest.permission.ACCESS_FINE_LOCATION
    )//读取SDCard权限
    // Manifest.permission.READ_PHONE_STATE,//读取手机状态权限
    var NO_STORAGE_PERMISSION: MutableList<String> = ArrayList()
    const val REQUEST_STORAGE = 0
}