package com.running.penguin.utils.system;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.running.penguin.R;
import com.running.penguin.permission.Permission;
import com.running.penguin.utils.Utils;
import com.running.penguin.utils.log.LogUtils;
import com.running.penguin.utils.other.IntentUtils;
import com.running.penguin.view.zloading.ZLoadingDialog;
import com.running.penguin.view.zloading.Z_TYPE;

import java.util.UUID;

/**
 * 程序信息工具集
 */

public class AppUtils {

    /**
     * 不允初始化
     */
    private AppUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * 判断App是否安装
     *
     * @param packageName 包名
     * @return {@code true}: 已安装<br>{@code false}: 未安装
     */
    public static boolean isInstallApp(String packageName) {
        return !isSpace(packageName) && IntentUtils.getLaunchAppIntent(packageName) != null;
    }


    /**
     * 获取客户端版本号
     *
     * @param context 上线问
     * @return int    版本号
     */
    public static int getVersionCode(Context context) {
        int versionCode;
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo packageInfo = pm.getPackageInfo(context.getPackageName(), 0);
            versionCode = packageInfo.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
            versionCode = 999;
        }
        return versionCode;
    }

    /**
     * 得到当前应用版本号名称
     *
     * @param context 上下文
     * @return 版本名，如：1.0.0
     */
    public static String getAppVersionName(Context context) {
        String versionName = "";
        try {
            // ---get the package info---
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context
                    .getPackageName(), 0);
            versionName = pi.versionName;
            if (versionName == null || versionName.length() <= 0) {
                return "";
            }
        } catch (Exception e) {
            Log.e("VersionInfo", "Exception", e);
        }
        return versionName;
    }

    @SuppressLint("LogNotTimber")
    public static String getAppVersionName(Context context, String replace) {
        String versionName = "";
        try {
            // ---get the package info---
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context
                    .getPackageName(), 0);
            versionName = pi.versionName;
            if (versionName == null || versionName.length() <= 0) {
                return "";
            }
        } catch (Exception e) {
            Log.e("VersionInfo", "Exception", e);
        }
        return versionName.replaceAll("\\.", "_");
    }


    /**
     * 获取App信息
     * <p>AppInfo（名称，图标，包名，版本号，版本Code，是否系统应用）</p>
     *
     * @param packageName 包名
     * @return 当前应用的AppInfo
     */
    public static AppInfo getAppInfo(String packageName) {
        try {
            PackageManager pm = Utils.getContext().getPackageManager();
            PackageInfo pi = pm.getPackageInfo(packageName, 0);
            return getBean(pm, pi);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 得到AppInfo的Bean
     *
     * @param pm 包的管理
     * @param pi 包的信息
     * @return AppInfo类
     */
    private static AppInfo getBean(PackageManager pm, PackageInfo pi) {
        if (pm == null || pi == null) return null;
        ApplicationInfo ai = pi.applicationInfo;
        String packageName = pi.packageName;
        String name = ai.loadLabel(pm).toString();
        Drawable icon = ai.loadIcon(pm);
        String packagePath = ai.sourceDir;
        String versionName = pi.versionName;
        int versionCode = pi.versionCode;
        boolean isSystem = (ApplicationInfo.FLAG_SYSTEM & ai.flags) != 0;
        return new AppInfo(packageName, name, icon, packagePath, versionName, versionCode, isSystem);
    }

    /**
     * 封装App信息的Bean类
     */
    public static class AppInfo {

        private String name;
        private Drawable icon;
        private String packageName;
        private String packagePath;
        private String versionName;
        private int versionCode;
        private boolean isSystem;

        public Drawable getIcon() {
            return icon;
        }

        public void setIcon(Drawable icon) {
            this.icon = icon;
        }

        public boolean isSystem() {
            return isSystem;
        }

        public void setSystem(boolean isSystem) {
            this.isSystem = isSystem;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getPackagePath() {
            return packagePath;
        }

        public void setPackagePath(String packagePath) {
            this.packagePath = packagePath;
        }

        public int getVersionCode() {
            return versionCode;
        }

        public void setVersionCode(int versionCode) {
            this.versionCode = versionCode;
        }

        public String getVersionName() {
            return versionName;
        }

        public void setVersionName(String versionName) {
            this.versionName = versionName;
        }

        /**
         * @param name        名称
         * @param icon        图标
         * @param packageName 包名
         * @param packagePath 包路径
         * @param versionName 版本号
         * @param versionCode 版本码
         * @param isSystem    是否系统应用
         */
        public AppInfo(String packageName, String name, Drawable icon, String packagePath,
                       String versionName, int versionCode, boolean isSystem) {
            this.setName(name);
            this.setIcon(icon);
            this.setPackageName(packageName);
            this.setPackagePath(packagePath);
            this.setVersionName(versionName);
            this.setVersionCode(versionCode);
            this.setSystem(isSystem);
        }

        @Override
        public String toString() {
            return "pkg name: " + getPackageName() +
                    "\napp name: " + getName() +
                    "\napp path: " + getPackagePath() +
                    "\napp v name: " + getVersionName() +
                    "\napp v code: " + getVersionCode() +
                    "\nis system: " + isSystem();
        }
    }

    /**
     * 获取App信息
     * <p>AppInfo（名称，图标，包名，版本号，版本Code，是否系统应用）</p>
     *
     * @return 当前应用的AppInfo
     */
    public static AppInfo getAppInfo() {
        return getAppInfo(Utils.getContext().getPackageName());
    }

    /**
     * 是否为空白字符串
     *
     * @param s 文本
     * @return true：文本为空白字符串
     */
    private static boolean isSpace(String s) {
        if (s == null) {
            return true;
        }
        for (int i = 0, len = s.length(); i < len; ++i) {
            if (!Character.isWhitespace(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * 获取手机设备号
     *
     * @param context
     * @return
     */
    public static String getSN(Context context) {
        return getUniqueDeviceID(context);
    }

    /**
     * @param context
     * @return 设备的唯一标识符
     */
    private static String serial = null;

    private static String getUniqueDeviceID(Context context) {

        String mSzdevidshort = "35" + Build.BOARD.length() % 10 + Build.BRAND.length() % 10 + Build.CPU_ABI.length() % 10
                + Build.DEVICE.length() % 10 + Build.DISPLAY.length() % 10 + Build.HOST.length() % 10
                + Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10
                + Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 + Build.TAGS.length() % 10
                + Build.TYPE.length() % 10 + Build.USER.length() % 10; //13 位
        try {
            serial = Build.class.getField("SERIAL").get(null).toString();
            return new UUID(mSzdevidshort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            Permission.with(context)
                    .runtime(Manifest.permission.READ_PHONE_STATE)
                    .rationale((context1, permissions, executor) -> executor.execute())
                    .onDenied(permissions -> LogUtils.e("no permission READ_PHONE_STATE")).onGranted(permissions -> {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                if (telephonyManager != null) {
                    @SuppressLint({"MissingPermission", "HardwareIds"})
                    String deviceId = telephonyManager.getDeviceId();
                    serial = "serial:" + deviceId;
                }
            });

        }
        return new UUID(mSzdevidshort.hashCode(), serial.hashCode()).toString();
    }

    /**
     * 获取apk版本号
     *
     * @param apkPath apkPath
     * @return apkVersionName
     */
    public static AppInfo getApkInfo(String apkPath) {
        if (apkPath != null && !apkPath.isEmpty()) {
            PackageManager pm = Utils.getContext().getPackageManager();
            PackageInfo info = pm.getPackageArchiveInfo(apkPath, PackageManager.GET_ACTIVITIES);

            return getBean(pm, info);
        }

        return null;
    }

    /**
     * 拨打电话
     * @param phoneNum 电话号码
     * @param mContext
     * @param isPlay true 跳转到拨号界面，用户手动点击拨打
     *               false 直接拨呼叫
     */
    public static void callPhoneNow(Context mContext,String phoneNum,boolean isPlay){
        Intent intent = new Intent(isPlay ? Intent.ACTION_DIAL : Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNum));
        mContext.startActivity(intent);
    }

    /**
     * 进度动画加载
     * @param mActivity
     * @return
     */
    public static ZLoadingDialog LoadingDialog(Activity mActivity,int color,Z_TYPE type){
        ZLoadingDialog dialog = new ZLoadingDialog(mActivity);
        int mainColorId = mActivity.getResources().getColor((color == 0) ? R.color.title_bg : color);
        dialog.setLoadingBuilder(type == null ? Z_TYPE.STAR_LOADING : type)
                .setLoadingColor(mainColorId) //设置加载动画的主色
                .setHintText("正在加载中...") //设置加载文字
                .setHintTextSize(16) // 设置字体大小
                .setHintTextColor(mainColorId) //设置字体颜色
                .setDurationTime(0.5) // 设置动画时间百分比
                .setDialogBackgroundColor(Color.WHITE); //设置背景色
        return dialog;
    }

}
