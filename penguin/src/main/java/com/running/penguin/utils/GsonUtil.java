package com.running.penguin.utils;

import com.google.gson.Gson;

/**
 * Created by reiserx on 2018/3/29.
 * desc :Gson单例
 */
public class GsonUtil {
    private static Gson gson;

    public static synchronized Gson gson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }
}
