package com.running.penguin.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import com.eftimoff.androipathview.PathView;
import com.running.penguin.R;

/**
*@Author : zlang-pc
*@Createtime : 2020/6/11 14:10
*@Describe : 吐司相关工具类
*/
public class ToastUtils {
    public static final int SUCCESS = 1;
    public static final int WARNING = 2;
    public static final int ERROR = 3;

    public static final int ANIM_TYPE_CENTER = 1;
    public static final int ANIM_TYPE_BOTTOM = 2;
    public static final int ANIM_TYPE_LEFT = 3;
    static PathView mPview;
    static AnimalToast mToast;
    private static BroadcastReceiver mHomeKeyEventReceiver = new BroadcastReceiver() {
        String SYSTEM_REASON = "reason";
        String SYSTEM_HOME_KEY = "homekey";
        String SYSTEM_HOME_KEY_LONG = "recentapps";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(SYSTEM_REASON);
                 if (TextUtils.equals(reason, SYSTEM_HOME_KEY)) {
                    //表示按了home键,程序到了后台
                    context.unregisterReceiver(mHomeKeyEventReceiver);
                    cancel();
                } else if (TextUtils.equals(reason, SYSTEM_HOME_KEY_LONG)) {
                    //表示长按home键,显示最近使用的程序列表
                    context.unregisterReceiver(mHomeKeyEventReceiver);
                    cancel();
                }
            }
        }
    };

    public static void cancel() {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        if (mPview != null) {
            mPview = null;
        }
    }

    private ToastUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    private static Toast sToast;
    private static Handler sHandler = new Handler(Looper.getMainLooper());
    private static boolean isJumpWhenMore;


    private static AnimalToast getInstance(Context context, @Nullable int animlocation) {
        if (mToast == null) {
            synchronized (AnimalToast.class) {
                if (mToast == null) {
                    mToast = new AnimalToast(context, animlocation);
                }
            }
        }

        mToast.setanim(animlocation);
        mToast.init();
        context.getApplicationContext().registerReceiver(mHomeKeyEventReceiver, new IntentFilter(
                Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        return mToast;
    }

    public static void makeText(Context context, String msg, int type) {
        mToast = getInstance(context, R.style.myToastScale);
        View layout = LayoutInflater.from(context).inflate(R.layout.toast_layout, null, false);
        switch (type) {
        case SUCCESS:
            layout.findViewById(R.id.base_layout).setBackgroundResource(R.drawable.background_success_toast);
            setLyout(msg, layout, R.drawable.success_toast, type);
            break;
        case WARNING:
            layout.findViewById(R.id.base_layout).setBackgroundResource(R.drawable.background_warning_toast);
            setLyout(msg, layout, R.drawable.warning_toast, type);
            break;
        case ERROR:
            layout.findViewById(R.id.base_layout).setBackgroundResource(R.drawable.background_error_toast);
            setLyout(msg, layout, R.drawable.error_toast, type);
            break;
        }
        mToast.setView(layout);
        mToast.show();
    }

    public static void makeText(Context context, String msg, int type, int animType) {
        switch (animType) {
        case ANIM_TYPE_LEFT:
            mToast = getInstance(context, R.style.myToastLeftRight);
            break;
        case ANIM_TYPE_CENTER:
            mToast = getInstance(context, R.style.myToastScale);
            break;
        case ANIM_TYPE_BOTTOM:
            mToast = getInstance(context, R.style.myToastUpDown);
            break;
        }
        View layout = LayoutInflater.from(context).inflate(R.layout.toast_layout, null, false);
        switch (type) {
        case SUCCESS:
            layout.findViewById(R.id.base_layout).setBackgroundResource(R.drawable.background_success_toast);
            setLyout(msg, layout, R.drawable.success_toast, type);
            break;
        case WARNING:
            layout.findViewById(R.id.base_layout).setBackgroundResource(R.drawable.background_warning_toast);
            setLyout(msg, layout, R.drawable.warning_toast, type);
            break;
        case ERROR:
            layout.findViewById(R.id.base_layout).setBackgroundResource(R.drawable.background_error_toast);
            setLyout(msg, layout, R.drawable.error_toast, type);
            break;
        }
        mToast.setView(layout);
        mToast.show();
    }

    private static void setLyout(String msg, View layout, int textRs, int type) {
        TextView text =  layout.findViewById(R.id.toastMessage);
        text.setText(msg);
        mPview = (PathView) layout.findViewById(R.id.pathview);
        switch (type) {
        case SUCCESS:
            mPview.setSvgResource(R.raw.s);
            break;
        case WARNING:
            mPview.setSvgResource(R.raw.w);
            break;
        case ERROR:
            mPview.setSvgResource(R.raw.e);
            break;
        }
        mPview.setFillAfter(true);
        mPview.useNaturalColors();
        mPview.getPathAnimator()
                .delay(200)
                .duration(700)
                .interpolator(new AccelerateDecelerateInterpolator())
                .start();
        text.setBackgroundResource(textRs);
        text.setTextColor(Color.parseColor("#FFFFFF"));
        mToast.setDuration(Toast.LENGTH_SHORT);
    }


    /***
     * 吐司初始化
     *
     * @param isJumpWhenMore 当连续弹出吐司时，是要弹出新吐司还是只修改文本内容
     *                       <p>{@code true}: 弹出新吐司<br>{@code false}: 只修改文本内容</p>
     *                       <p>如果为{@code false}的话可用来做显示任意时长的吐司</p>
     */
    public static void init(boolean isJumpWhenMore) {
        ToastUtils.isJumpWhenMore = isJumpWhenMore;
    }

    /***
     * 安全地显示短时吐司
     *
     * @param text 文本
     */
    public static void showShortToastSafe(final CharSequence text) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(text, Toast.LENGTH_SHORT);
            }
        });
    }

    /***
     * 安全地显示短时吐司
     *
     * @param resId 资源Id
     */
    public static void showShortToastSafe(final @StringRes int resId) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(resId, Toast.LENGTH_SHORT);
            }
        });
    }

    /***
     * 安全地显示短时吐司
     *
     * @param resId 资源Id
     * @param args  参数
     */
    public static void showShortToastSafe(final @StringRes int resId, final Object... args) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(resId, Toast.LENGTH_SHORT, args);
            }
        });
    }

    /***
     * 安全地显示短时吐司
     *
     * @param format 格式
     * @param args   参数
     */
    public static void showShortToastSafe(final String format, final Object... args) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(format, Toast.LENGTH_SHORT, args);
            }
        });
    }

    /***
     * 安全地显示长时吐司
     *
     * @param text 文本
     */
    public static void showLongToastSafe(final CharSequence text) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(text, Toast.LENGTH_LONG);
            }
        });
    }

    /***
     * 安全地显示长时吐司
     *
     * @param resId 资源Id
     */
    public static void showLongToastSafe(final @StringRes int resId) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(resId, Toast.LENGTH_LONG);
            }
        });
    }

    /***
     * 安全地显示长时吐司
     *
     * @param resId 资源Id
     * @param args  参数
     */
    public static void showLongToastSafe(final @StringRes int resId, final Object... args) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(resId, Toast.LENGTH_LONG, args);
            }
        });
    }

    /***
     * 安全地显示长时吐司
     *
     * @param format 格式
     * @param args   参数
     */
    public static void showLongToastSafe(final String format, final Object... args) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(format, Toast.LENGTH_LONG, args);
            }
        });
    }

    /***
     * 显示短时吐司
     *
     * @param text 文本
     */
    public static void showShortToast(CharSequence text) {
        if(TextUtils.isEmpty(text)) {
            return;
        }
        makeText(Utils.getContext(), text.toString(), ERROR);
    }

    /***
     * 显示短时吐司
     *
     * @param resId 资源Id
     */
    public static void showShortToast(@StringRes int resId) {
        makeText(Utils.getContext(), Utils.getContext().getResources().getText(resId).toString(), ERROR);
    }

    /***
     * 成功
     *
     * @param text
     */
    public static void showSuccessToast(CharSequence text) {
        if(TextUtils.isEmpty(text)) {
            return;
        }
        makeText(Utils.getContext(), text.toString(), SUCCESS);
    }


    /***
     * 失败
     *
     * @param text
     */
    public static void showErrorToast(CharSequence text) {
        if(TextUtils.isEmpty(text)) {
            return;
        }
        makeText(Utils.getContext(), text.toString(), ERROR);
    }

    /***
     * 警告
     *
     * @param text
     */
    public static void showWarningToast(CharSequence text) {
        if(TextUtils.isEmpty(text)) {
            return;
        }
        makeText(Utils.getContext(), text.toString(), WARNING);
    }

    /***
     * 显示短时吐司
     *
     * @param resId 资源Id
     * @param args  参数
     */
    public static void showShortToast(@StringRes int resId, Object... args) {
        showToast(resId, Toast.LENGTH_SHORT, args);
    }

    /***
     * 显示短时吐司
     *
     * @param format 格式
     * @param args   参数
     */
    public static void showShortToast(String format, Object... args) {
        showToast(format, Toast.LENGTH_SHORT, args);
    }

    /***
     * 显示长时吐司
     *
     * @param text 文本
     */
    public static void showLongToast(CharSequence text) {
        showToast(text, Toast.LENGTH_LONG);
    }

    /***
     * 显示长时吐司
     *
     * @param resId 资源Id
     */
    public static void showLongToast(@StringRes int resId) {
        showToast(resId, Toast.LENGTH_LONG);
    }

    /***
     * 显示长时吐司
     *
     * @param resId 资源Id
     * @param args  参数
     */
    public static void showLongToast(@StringRes int resId, Object... args) {
        showToast(resId, Toast.LENGTH_LONG, args);
    }

    /***
     * 显示长时吐司
     *
     * @param format 格式
     * @param args   参数
     */
    public static void showLongToast(String format, Object... args) {
        showToast(format, Toast.LENGTH_LONG, args);
    }

    /***
     * 显示吐司
     *
     * @param resId    资源Id
     * @param duration 显示时长
     */
    private static void showToast(@StringRes int resId, int duration) {
        showToast(Utils.getContext().getResources().getText(resId).toString(), duration);
    }

    /***
     * 显示吐司
     *
     * @param resId    资源Id
     * @param duration 显示时长
     * @param args     参数
     */
    private static void showToast(@StringRes int resId, int duration, Object... args) {
        showToast(String.format(Utils.getContext().getResources().getString(resId), args), duration);
    }

    /***
     * 显示吐司
     *
     * @param format   格式
     * @param duration 显示时长
     * @param args     参数
     */
    private static void showToast(String format, int duration, Object... args) {
        showToast(String.format(format, args), duration);
    }

    /***
     * 显示吐司
     *
     * @param text     文本
     * @param duration 显示时长
     */
    private static void showToast(CharSequence text, int duration) {
        if (isJumpWhenMore) {
            cancelToast();
        }
        if (sToast == null) {
            sToast = Toast.makeText(Utils.getContext(), text, duration);
        } else {
            sToast.setText(text);
            sToast.setDuration(duration);
        }
        sToast.show();
    }

    /***
     * 取消吐司显示
     */
    public static void cancelToast() {
        if (sToast != null) {
            sToast.cancel();
            sToast = null;
        }
    }

    public static void cancemlToast() {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
    }
}