package com.running.penguin.utils.assist

import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by ZhaoShulin on 2019-05-22 17:12.
 * <br>
 * Desc:网络相关工具
 * <br>
 */
/**
 * 获取ConnectivityManager
 */
fun Context.getConnectivityManager(): ConnectivityManager {
    return this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
}

/**
 * 判断网络连接是否有效（此时可传输数据
 * @return boolean 不管wifi，还是mobile net，只有当前在连接状态（可有效传输数据）才返回true,反之false。
 */
fun Context.isConnected(): Boolean {
    val net = this.getConnectivityManager().activeNetworkInfo
    return net != null && net.isAvailable
}