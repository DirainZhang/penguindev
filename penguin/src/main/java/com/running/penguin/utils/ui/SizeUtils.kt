package com.running.penguin.utils.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup

/**
 * Created by ZhaoShulin on 2019-05-22 15:57.
 * <br>
 * Desc: 尺寸相关工具类
 * <br>
 */

private val metrics by lazy { Resources.getSystem().displayMetrics }

/**
 * dp转px
 * @return px值
 */
fun Float.dp2px(): Int {
    return (this * metrics.density + 0.5f).toInt()
}

/**
 * px转dp
 * @return dp值
 */
fun Float.px2dp(): Int {
    return (this / metrics.density + 0.5f).toInt()
}

/**
 * sp转px
 * @return px值
 */
fun Float.sp2px(): Int {
    return (this * metrics.scaledDensity + 0.5f).toInt()
}

/**
 * px转sp
 * @return sp值
 */
fun Float.px2sp(): Int {
    return (this / metrics.scaledDensity + 0.5f).toInt()
}

/**
 * 各种单位转换
 *
 * 该方法存在于TypedValue
 *
 * @param unit    单位
 * @param value   值
 * @param metrics DisplayMetrics
 * @return 转换结果
 */
fun applyDimension(unit: Int, value: Float, metrics: DisplayMetrics): Float {
    when (unit) {
        TypedValue.COMPLEX_UNIT_PX -> return value
        TypedValue.COMPLEX_UNIT_DIP -> return value * metrics.density
        TypedValue.COMPLEX_UNIT_SP -> return value * metrics.scaledDensity
        TypedValue.COMPLEX_UNIT_PT -> return value * metrics.xdpi * (1.0f / 72)
        TypedValue.COMPLEX_UNIT_IN -> return value * metrics.xdpi
        TypedValue.COMPLEX_UNIT_MM -> return value * metrics.xdpi * (1.0f / 25.4f)
    }
    return 0f
}

/**
 * 在onCreate中获取视图的尺寸
 *
 * 需回调onGetSizeListener接口，在onGetSize中获取view宽高
 *
 * 用法示例如下所示
 * <pre>
 * SizeUtils.forceGetViewSize(view, new SizeUtils.onGetSizeListener() {
 * Override
 * public void onGetSize(View view) {
 * view.getWidth();
 * }
 * });
 *
 * forceGetViewSize(fab){
 * it.width
 * }
 *
</pre> *
 *
 * @param view     视图
 * @param listener 获取到View尺寸的监听
 */
fun forceGetViewSize(view: View, listener: (v: View) -> Unit) {
    view.post {
        listener(view)
    }
}


/**
 * 测量视图尺寸
 *
 * @param view 视图
 * @return arr[0]: 视图宽度, arr[1]: 视图高度
 */
fun measureView(view: View): IntArray {
    var lp: ViewGroup.LayoutParams? = view.layoutParams
    if (lp == null) {
        lp = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }
    val widthSpec = ViewGroup.getChildMeasureSpec(0, 0, lp.width)
    val lpHeight = lp.height
    val heightSpec = if (lpHeight > 0) {
        View.MeasureSpec.makeMeasureSpec(lpHeight, View.MeasureSpec.EXACTLY)
    } else {
        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
    }
    view.measure(widthSpec, heightSpec)
    return intArrayOf(view.measuredWidth, view.measuredHeight)
}

/**
 * 获取测量视图宽度
 *
 * @param view 视图
 * @return 视图宽度
 */
fun getMeasuredWidth(view: View): Int {
    return measureView(view)[0]
}

/**
 * 获取测量视图高度
 *
 * @param view 视图
 * @return 视图高度
 */
fun getMeasuredHeight(view: View): Int {
    return measureView(view)[1]
}

/**
 * 獲取當前屏幕的寬度
 *
 * @param context context
 * @return 当前屏幕宽度
 */
fun getScreenWidth(): Int {
    return Resources.getSystem().displayMetrics.widthPixels
}

/**
 * 獲取當前屏幕的高度
 *
 * @param context context
 * @return 手机屏幕的高度(px)，包含statusbar
 */

fun getScreenHeight(): Int {
    return Resources.getSystem().displayMetrics.heightPixels
}

@SuppressLint("PrivateApi")
fun getStatusHeight(context: Context): Int {
    var statusHeight = -1
    try {
        val clazz = Class.forName("com.android.internal.R\$dimen")
        val `object` = clazz.newInstance()
        val height = Integer.parseInt(clazz.getField("status_bar_height").get(`object`).toString())
        statusHeight = context.resources.getDimensionPixelSize(height)
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return statusHeight
}