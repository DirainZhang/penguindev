package com.running.penguin.utils;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.running.penguin.R;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class GlideUtil {

    private static GlideUtil imageLoadUtil;

    private GlideUtil() {
    }

    public static GlideUtil getInstance() {
        if (imageLoadUtil == null) {
            synchronized (GlideUtil.class) {
                if (imageLoadUtil == null)
                    imageLoadUtil = new GlideUtil();
                return imageLoadUtil;
            }

        }
        return imageLoadUtil;
    }

    private int defaultIconRes = R.drawable.placeholder_img_square;
    private int defaultAvatarRes = R.drawable.placeholder_avatar;

    public void loadUrlSource(ImageView imageView, String imgUrl, @DrawableRes int placeHolder) {

        Glide.with(Utils.getContext())
                .asBitmap()
                .load(imgUrl)
                .apply(new RequestOptions().placeholder(placeHolder).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                .into(imageView);

    }

    public void loadUrlSourceCenterCrop(ImageView imageView, String imgUrl, @DrawableRes int placeHolder) {
        Glide.with(Utils.getContext())
                .asBitmap()
                .load(imgUrl)
                .apply(new RequestOptions().placeholder(placeHolder).diskCacheStrategy(DiskCacheStrategy.RESOURCE).centerCrop())
                .into(imageView);
    }

    public void loadUrl(ImageView imageView, String imgUrl) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .apply(new RequestOptions().placeholder(defaultIconRes).error(defaultIconRes))

                .into(imageView);
    }

    public void loadUrl(ImageView imageView, @DrawableRes int imgUrl) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .into(imageView);
    }

    public void loadUrl(ImageView imageView, String imgUrl, int placeHolder) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .apply(new RequestOptions().placeholder(placeHolder).error(placeHolder).dontAnimate())
                .into(imageView);
    }

    public void loadUrlCrossFade(ImageView imageView, String imgUrl) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .transition(new DrawableTransitionOptions().crossFade())
                .into(imageView);
    }

    public void loadUrlCrossFade(ImageView imageView, String imgUrl, @DrawableRes int placeHolder) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .transition(new DrawableTransitionOptions().crossFade())
                .apply(new RequestOptions().placeholder(placeHolder))
                .into(imageView);
    }

    public void loadUrlFitCenter(ImageView imageView, String imgUrl) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .apply(new RequestOptions().placeholder(defaultIconRes).error(defaultIconRes).fitCenter())
                .into(imageView);
    }

    public void loadUrlCenterCrop(ImageView imageView, String imgUrl) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .apply(new RequestOptions().placeholder(defaultIconRes).error(defaultIconRes).centerCrop())
                .into(imageView);
    }


    public void loadHeadUrlcircle(ImageView imageView, String imgUrl) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .apply(bitmapTransform(new CircleCrop()).placeholder(R.drawable.placeholder_avatar).error(R.drawable.placeholder_avatar).dontAnimate())
                .into(imageView);

    }

    public void loadHeadUrlcircleWithmask(ImageView imageView, String imgUrl) {
        imageView.setColorFilter(Color.parseColor("#80FFFFFF"), PorterDuff.Mode.MULTIPLY);
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .apply(bitmapTransform(new CircleCrop()).placeholder(R.drawable.placeholder_avatar).error(R.drawable.placeholder_avatar).dontAnimate())
                .into(imageView);
    }

    public void loadImgUrlWithoutDefault(ImageView imageView, String imgUrl) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .into(imageView);
    }

    public void loadImgUrlWithoutDefault(ImageView imageView, @DrawableRes int imgUrl) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .into(imageView);
    }

    public void loadGifUrl(ImageView imageView, @DrawableRes int gifRes) {
        Glide.with(Utils.getContext())
                .asGif()
                .load(gifRes)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).centerCrop())
                .into(imageView);
    }

    public void loadGifUrl(ImageView imageView, File file) {
        Glide.with(Utils.getContext()).asGif().load(file).into(imageView);
    }

    public void loadGifUrl(ImageView imageView, File file, @DrawableRes int Res) {
        Glide.with(Utils.getContext())
                .load(file)
                .apply(new RequestOptions().placeholder(Res).error(Res))
                .into(imageView);
    }

    public void loadAvatarUrl(ImageView imageView, String imgUrl) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .apply(new RequestOptions().placeholder(defaultAvatarRes).error(defaultAvatarRes))
                .into(imageView);
    }

    public void loadImgUrl(ImageView imageView, String imgUrl) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .apply(new RequestOptions().placeholder(defaultIconRes).error(defaultIconRes).dontAnimate())
                .into(imageView);

    }

    public void loadImgUrl(ImageView imageView, String imgUrl, @DrawableRes int placeHolder) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .apply(new RequestOptions().placeholder(placeHolder).error(placeHolder).dontAnimate())
                .into(imageView);

    }

    public void loadUrlSimpleTarget(String imgUrl, final GlideUtilSimpleTarget target) {
        Glide.with(Utils.getContext()).load(imgUrl).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                target.onResourceReady(resource);
            }

            @Override
            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                super.onLoadFailed(errorDrawable);
                target.onLoadFailed();
            }
        });

    }

    public void loadUrlDiskCacheNONE(ImageView imageView, String url, @DrawableRes int placeHolder) {
        Glide.with(Utils.getContext())
                .load(url)
                .apply(new RequestOptions().error(placeHolder).diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(imageView);
    }

    public interface GlideUtilSimpleTarget {
        void onResourceReady(Drawable resource);

        void onLoadFailed();
    }

    public Bitmap loadUrlgetBitmap(String imgUrl, int size) throws InterruptedException, ExecutionException, TimeoutException {

        return Glide.with(Utils.getContext())
                .asBitmap()
                .load(imgUrl)
                .apply(new RequestOptions()
                        .centerCrop()
                        .override(size, size))
                .submit()
                .get(200, TimeUnit.MILLISECONDS);// 最大等待200ms
    }

    public void loadUrl(ImageView image, String imgUrl, @DrawableRes int defaultResId, int width, int height) {

        Glide.with(Utils.getContext()).asBitmap()
                .load(imgUrl)
                .apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(defaultResId)
                        .error(defaultResId)
                        .centerCrop()
                        .override(width, height))
                .into(image);
    }

    public void loadUrltoCache(String imgUrl, int size) {
        Glide.with(Utils.getContext())
                .load(imgUrl)
                .submit(size, size);
    }
}
