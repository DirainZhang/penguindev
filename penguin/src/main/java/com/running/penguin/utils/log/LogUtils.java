package com.running.penguin.utils.log;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.running.penguin.BuildConfig;


/**
 * desc :日志工具
 */

public class LogUtils {
    private static String DEFAULT_TAG = "LogUtils";
    private static boolean opLog = BuildConfig.DEBUG;

    /**
     * 日志工具类，不允许初始化
     */
    private LogUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * @param openLog 是否开启log
     */
    public static void init(boolean openLog, String tag) {
        opLog = openLog;
        DEFAULT_TAG = tag;
    }

    /**
     * 带标签的Debug级日志
     *
     * @param tag 标签
     * @param str 日志内容
     */
    public static void d(String tag, String str) {
        if (!opLog) return;
        if (TextUtils.isEmpty(str)) return;
        if (TextUtils.isEmpty(tag)) {
            DebugLogger.d(DEFAULT_TAG, str);
        } else {
            DebugLogger.d(tag, str);
        }
    }

    /**
     * debug级日志
     *
     * @param str 日志内容
     */
    public static void d(String str) {
        if (!opLog) return;
        if (!TextUtils.isEmpty(str)) {
            DebugLogger.d(DEFAULT_TAG, str);
        }
    }

    public static void d(Object object) {
        d(new Gson().toJson(object));
    }

    /**
     * 带标签的Error级日志
     *
     * @param tag 标签
     * @param str 日志内容
     */
    public static void e(String tag, String str) {
        if (!opLog) return;
        if (TextUtils.isEmpty(str)) return;
        if (TextUtils.isEmpty(tag)) {
            DebugLogger.e(DEFAULT_TAG, str);
        } else {
            DebugLogger.e(tag, str);
        }
    }

    /**
     * Error级日志
     *
     * @param str 日志内容
     */
    public static void e(String str) {
        if (!opLog) return;
        if (TextUtils.isEmpty(str)) return;
        DebugLogger.e(DEFAULT_TAG, str);
    }


    /**
     * Error级日志
     *
     * @param e 错误
     */
    public static void e(Exception e) {
        if (!opLog) return;
        if (e != null) {
            DebugLogger.e(DEFAULT_TAG, e.getMessage());
        }
    }

    /**
     * 带标签的warn级日志
     *
     * @param tag 标签
     * @param str 日志内容
     */
    public static void w(String tag, String str) {
        if (!opLog) return;
        if (TextUtils.isEmpty(str)) return;
        if (TextUtils.isEmpty(tag)) {
            DebugLogger.w(DEFAULT_TAG, str);
        } else {
            DebugLogger.w(tag, str);
        }
    }

    /**
     * warn级日志
     *
     * @param str 日志内容
     */
    public static void w(String str) {
        if (!opLog) return;
        if (TextUtils.isEmpty(str)) return;
        DebugLogger.w(DEFAULT_TAG, str);
    }

    /**
     * 带标签的 verbose 级日志
     *
     * @param tag 标签
     * @param str 日志内容
     */
    public static void v(String tag, String str) {
        if (!opLog) return;
        if (TextUtils.isEmpty(str)) return;
        if (TextUtils.isEmpty(tag)) {
            DebugLogger.v(DEFAULT_TAG, str);
        } else {
            DebugLogger.v(tag, str);
        }
    }

    /**
     * verbose 级日志
     *
     * @param str 日志内容
     */
    public static void v(String str) {
        if (!TextUtils.isEmpty(str)) {
            DebugLogger.v(DEFAULT_TAG, str);
        }
    }

    /**
     * info级日志
     *
     * @param tag tag
     * @param str 日志内容
     */
    public static void i(String tag, String str) {
        if (!opLog) return;
        if (TextUtils.isEmpty(str)) return;
        if (TextUtils.isEmpty(tag)) {
            DebugLogger.i(DEFAULT_TAG, str);
        } else {
            DebugLogger.i(tag, str);
        }
    }

    /**
     * info级日志
     *
     * @param str 日志内容
     */
    public static void i(String str) {
        if (!TextUtils.isEmpty(str)) {
            DebugLogger.i(DEFAULT_TAG, str);
        }
    }

    /**
     * wtf 级日志
     *
     * @param tag tag
     * @param str 日志内容
     */
    public static void wtf(String tag, String str) {
        if (!opLog) return;
        if (TextUtils.isEmpty(str)) return;
        if (TextUtils.isEmpty(tag)) {
            DebugLogger.wtf(DEFAULT_TAG, str);
        } else {
            DebugLogger.wtf(tag, str);
        }
    }

    /**
     * wtf 级日志
     *
     * @param str 日志内容
     */
    public static void wtf(String str) {
        if (!TextUtils.isEmpty(str)) {
            DebugLogger.wtf(DEFAULT_TAG, str);
        }
    }

    /**
     * 异常日志打印
     *
     * @param e   异常
     * @param str tag
     */
    public static void e(Throwable e, String str) {
        if (e != null && !TextUtils.isEmpty(str)) {
            DebugLogger.e(DEFAULT_TAG, str, e);
        }
    }


}
