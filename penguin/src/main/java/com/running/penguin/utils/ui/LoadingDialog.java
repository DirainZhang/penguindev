package com.running.penguin.utils.ui;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.StringRes;
import android.view.View;
import android.widget.TextView;
import com.running.penguin.R;


/***
 * Created on 2017/3/23
 * Author：qzj
 */
public class LoadingDialog extends Dialog {

    private Context context;
    View view;
    TextView loadingTV;

    String loadingText = "";

    public LoadingDialog(Context context) {
        super(context, R.style.TransDialog);
        this.context = context;
        view = View.inflate(context, R.layout.layout_pop_loading, null);
        loadingTV = view.findViewById(R.id.pop_loading_txt);

    }

    /**
     * 使用 {@link #setMsg(String)}代替
     *
     * @param text
     * @return
     */
    @SuppressWarnings("JavadocReference")
    @Deprecated
    public LoadingDialog setLoadingText(String text) {
        this.loadingText = text;
        return this;
    }

    private void setUIBeforeShow() {
        setMessage(loadingText);
    }

    private void setMessage(CharSequence loading) {
        if (loading == null || "".equals(this.loadingText)) {
            loadingTV.setVisibility(View.GONE);
        } else {
            loadingTV.setText(loading);
            loadingTV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void show() {
        setUIBeforeShow();
        super.show();
    }

    public Context getViewContext() {
        return context;
    }


    public void setMsg(CharSequence msg) {
        setMessage(msg);
    }

    public void setMsg(String msg) {
        setMessage(msg);
    }

    public void setMsg(@StringRes int msg) {
        setMessage(context.getString(msg));
    }

    /**
     * 弹出Dialog
     *
     * @param message    文本信息
     * @param cancelable 是可以点击返回键
     * @param listener   取消监听
     * @return Dialog
     */
    public Dialog build(CharSequence message, boolean cancelable, OnCancelListener listener) {
        setMessage(message);
        setCancelable(cancelable);
        setOnCancelListener(listener);
        setCancelable(cancelable);
        setCanceledOnTouchOutside(false);
        setContentView(view);
        this.show();
        return this;
    }

}
