package com.running.penguin.utils.log;

import android.annotation.SuppressLint;
import android.util.Log;
import com.running.penguin.BuildConfig;


/**
 * Created by huxinyu on 2018/9/10.
 * Email : panda.h@foxmail.com
 * <p>
 * Description : debug 模式下的 Logger，支持在 Log 信息中跳转 Log位置
 */
class DebugLogger {
    private static String className;//类名
    private static int lineNumber;//行数

    private DebugLogger() {
        /* Protect from instantiations */
    }

    private static String createLog(String log) {
        return "(" + className + ":" + lineNumber + ")" + "  Message ::: " + log;
    }

    private static void getMethodNames(StackTraceElement[] sElements) {
        className = sElements[2].getFileName();
        lineNumber = sElements[2].getLineNumber();
    }


    @SuppressLint("LogNotTimber")
    public static void e(String tag, String message) {
        if (!BuildConfig.DEBUG) {
            return;
        }
        // Throwable instance must be created before any methods
        getMethodNames(new Throwable().getStackTrace());
        Log.e(tag, createLog(message));
    }

    @SuppressLint("LogNotTimber")
    public static void e(String tag, String message,Throwable e) {
        if (!BuildConfig.DEBUG)
            return;
        // Throwable instance must be created before any methods
        getMethodNames(new Throwable().getStackTrace());
        Log.e(tag, createLog(message),e);
    }

    @SuppressLint("LogNotTimber")
    public static void i(String tag, String message) {
        if (!BuildConfig.DEBUG) {
            return;
        }
        getMethodNames(new Throwable().getStackTrace());
        Log.i(tag, createLog(message));
    }

    @SuppressLint("LogNotTimber")
    public static void d(String tag, String message) {
        if (!BuildConfig.DEBUG) {
            return;
        }
        getMethodNames(new Throwable().getStackTrace());
        Log.d(tag, createLog(message));
    }

    @SuppressLint("LogNotTimber")
    public static void v(String tag, String message) {
        if (!BuildConfig.DEBUG) {
            return;
        }
        getMethodNames(new Throwable().getStackTrace());
        Log.v(tag, createLog(message));
    }

    @SuppressLint("LogNotTimber")
    public static void w(String tag, String message) {
        if (!BuildConfig.DEBUG) {
            return;
        }
        getMethodNames(new Throwable().getStackTrace());
        Log.w(tag, createLog(message));
    }

    @SuppressLint("LogNotTimber")
    public static void wtf(String tag, String message) {
        if (!BuildConfig.DEBUG) {
            return;
        }
        getMethodNames(new Throwable().getStackTrace());
        Log.wtf(tag, createLog(message));
    }
}
