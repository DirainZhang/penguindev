package com.running.penguin.utils.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import com.running.penguin.R;


/**
 * Created by reiserx on 2017/8/15.
 * desc :loading
 */
public class ProgressDialogUtils {

    @SuppressLint("StaticFieldLeak")
    private static LoadingDialog mProgressDialog;


    /**
     * private
     */
    private ProgressDialogUtils() {
    }

    /**
     * 显示加载框
     *
     * @param context 上下文
     */
    public static void show(Context context) {
        showProgress(context, context.getString(R.string.srl_footer_loading), true);
    }

    /**
     * 显示设置取消的加载框
     *
     * @param context    上下文
     * @param cancelable 是否可以取消
     */
    public static void show(Context context, boolean cancelable) {
        showProgress(context, context.getString(R.string.srl_footer_loading), cancelable);
    }

    /**
     * 显示设置message的加载框
     *
     * @param context 上下文
     * @param message 文本内容
     */
    public static void show(Context context, CharSequence message) {
        showProgress(context, message, true);
    }

    /**
     * 显示设置 取消和文本 的加载框
     *
     * @param context    上下文
     * @param message    文本
     * @param cancelable 取消
     */
    public static void show(Context context, CharSequence message, boolean cancelable) {
        showProgress(context, message, cancelable);
    }


    /**
     * 显示设置 取消和文本 的加载框
     *
     * @param message    文本
     * @param cancelable 取消
     * @param context    上下文
     */
    public static void showProgress(Context context, CharSequence message, boolean cancelable) {
        showProgress(message, cancelable, context, null);
    }


    /**
     * 显示设置 取消和文本 的加载框
     *
     * @param message    文本
     * @param cancelable 取消
     * @param context    上下文
     */
    public static void showProgress(CharSequence message,
                                    boolean cancelable,
                                    Context context,
                                    DialogInterface.OnCancelListener listener) {
        //如果时当前页面正在显示
        if (isShowing() && contextIsSame(mProgressDialog, context)) {
            mProgressDialog.setMsg(message);
            return;
        }
        releaseDialog(context);
        mProgressDialog = new LoadingDialog(context);
        mProgressDialog.build(message, cancelable, listener);
    }


    /**
     * 加载框是否显示
     *
     * @return true 显示 false 未显示
     */
    public static boolean isShowing() {
        return mProgressDialog != null && mProgressDialog.isShowing();
    }

    /**
     * 隐藏加载框,延迟600ms
     */
    public static void hideProgress() {
        try {
            if (mProgressDialog == null) {
                return;
            }
            dissmissDialog(mProgressDialog.getViewContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 关闭dialog
     */
    private static void dissmissDialog(Context context) {
        try {
            if (isShowing() && contextIsSame(mProgressDialog, context)) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 直接消失
     */
    public static void hideProgressQuick(Context context) {
        if (mProgressDialog != null) {
            dissmissDialog(context);
        }
    }

    /**
     * 置空，避免引用activity导致内存泄露
     */
    public static void releaseDialog(Context context) {
        if (mProgressDialog != null && contextIsSame(mProgressDialog, context)) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            mProgressDialog = null;
        }
    }


    /**
     * 置空，避免引用activity导致内存泄露
     *
     * @param context 上下文
     * @param success 是否为成功
     * @param endAnim 是否有结束动画
     */
    public static void releaseDialog(Context context, boolean success, boolean endAnim) {
        if (mProgressDialog != null && contextIsSame(mProgressDialog, context)) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            mProgressDialog = null;
        }
    }


    /**
     * 是否在同一个页面
     *
     * @param dialog  GouuseDialog
     * @param context 待判断的contxt
     * @return true 同一个页面
     */
    private static boolean contextIsSame(LoadingDialog dialog, Context context) {
        try {
            return dialog.getViewContext().getClass().getSimpleName().equals(context.getClass().getSimpleName());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
