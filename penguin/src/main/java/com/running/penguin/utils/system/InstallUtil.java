package com.running.penguin.utils.system;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

import com.running.penguin.permission.Permission;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 安装APK，兼容了7.0及以上和root方式安装
 */
public class InstallUtil {
    /**
     * 安装工具类，不允许初始化
     */
    private InstallUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * @param context  上下文
     * @param apkPath  要安装的APK
     * @param rootMode 是否是Root模式
     */
    private static void install(Context context, String apkPath, boolean rootMode) {
        if (rootMode) {
            installRoot(context, apkPath);
        } else {
            installNormal(context, apkPath);
        }
    }

    /**
     * 通过非Root模式安装
     *
     * @param context 上下文
     * @param apkPath 安装路径
     */
    public static void install(Context context, String apkPath) {
        install(context, apkPath, false);
    }

    /**
     * 普通安装
     *
     * @param context 上下文
     * @param apkPath apk文件路径
     */
    private static void installNormal(Context context, String apkPath) {
        Permission.with(context)
                .install(apkPath)
                .onDenied(permissions -> Toast.makeText(context, "未授权安装应用", Toast.LENGTH_SHORT))
                .start();
    }

    /**
     * 可以直接使用.dispose()结束调用，但是或许存在一些未知风险
     * 多久才会dispose()放弃订阅？如果我之间有一个或多个map()或者其他，
     * 是否能有任何保证我会onCompletion()前收到订阅
     *
     * @param context Context
     * @param apkPath 安装包路径
     */
    //通过Root方式安装
    @SuppressLint("CheckResult")
    private static void installRoot(final Context context, final String apkPath) {
        Observable.just(apkPath)
                .map(mApkPath -> "pm install -r " + mApkPath)
                .map(SystemManager::RootCommand)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integer -> {
                    if (integer == 0) {
                        Toast.makeText(context, "安装成功", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "root权限获取失败,尝试普通安装", Toast.LENGTH_SHORT).show();
                        install(context, apkPath);
                    }
                });

    }
}
