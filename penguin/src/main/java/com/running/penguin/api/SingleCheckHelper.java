package com.running.penguin.api;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * @Author : zlang-pc
 * @Createtime : 2020/8/11 20:33
 * @Describe :
 */
public class SingleCheckHelper extends CheckHelper {
    private int checked = -1;

    public SingleCheckHelper(RecyclerView recyclerView) {
        super(recyclerView);
    }

    public SingleCheckHelper(RecyclerView recyclerView, int checked) {
        super(recyclerView);
        this.checked = checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    @Override
    void bindViewHolder(final RecyclerView.ViewHolder viewHolder, View clickView) {
        final int position = viewHolder.getAdapterPosition();
        clickView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean contain = isCheckedPosition(position);
                if (contain) {
                    handleChange(viewHolder, true);
                } else {
                    if (checked != -1) {
                        RecyclerView.ViewHolder holder = recyclerView.findViewHolderForAdapterPosition(checked);
                        if (holder != null) {
                            stateChange(holder, false);
                        }
                    }
                    checked = position;
                    stateChange(viewHolder, true);
                }
                clickWhich(position);
            }
        });
        stateChange(viewHolder, isCheckedPosition(position));
    }


    @Override
    boolean isCheckedPosition(int position) {
        return position == checked;
    }

}
