package com.running.penguin.api.exception;

import com.running.penguin.utils.NetWorkUtils;
import com.running.penguin.utils.Utils;

import java.io.IOException;

import retrofit2.HttpException;

/**
 * @Author : zlang-pc
 * @Createtime : 2020/6/2 10:38
 * @Describe :
 */
public class ExceptionEngine {

    public static ApiException handleException(Throwable e) {
        ApiException ex;
        if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
            ex = new ApiException(httpException, Error.HTTP_ERROR);
            ex.setErrorCode(httpException.code());
            ex.setMsg("服务器连接异常");
            return ex;
        } else if (e instanceof ServerException) {
            ServerException resultException = (ServerException) e;
            ex = new ApiException(resultException, Error.SERVER_ERROR);
            ex.setErrorCode(resultException.getCode());
            ex.setMsg(resultException.getMsg());
//            if (ex.getErrorCode() == Error.LOGIN_OUT&&!AppManager.getAppManager().topActivityIsLogin()) {
//                Data.exitAccount(Utils.getContext());
//                EventBus.getDefault().post(new UserLoginEvent(false));
//                ARouter.getInstance().build("/app/common/main").navigation();
//                JPushInterface.setAliasAndTags(Utils.getContext(), "", null, null);
//                ARouter.getInstance().build("/common/login").withString("SignOutType", SIGNOUTTYPE_SESSION).navigation();
//            }
            return ex;
        } else if (e instanceof IOException) {
            if (!NetWorkUtils.isNetworkConnected(Utils.getContext())) {
                ex = new ApiException(e, Error.NETWORK_ERROR);
                ex.setMsg("连接失败");
            } else {
                ex = new ApiException(e, Error.HTTP_ERROR);
                ex.setMsg("服务器连接异常");
            }

            return ex;
        } else if (e instanceof NullPointerException) {
            ex = new ApiException(e, Error.EMPTY);
            ex.setMsg("空指针");
            return ex;
        } else {
            ex = new ApiException(e, Error.UNKNOWN);
            ex.setMsg("未知错误");
            return ex;
        }
    }

}

