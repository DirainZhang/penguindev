package com.running.penguin.api;

import com.running.penguin.api.exception.ExceptionEngine;
import com.running.penguin.api.exception.ServerException;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * @Author : zlang-pc
 * @Createtime : 2020/6/2 10:36
 * @Describe :
 */
public class RxHelper {


    public static class ServerResultFunc<T> implements Function<ApiResult<T>, T> {
        @Override
        public T apply(ApiResult<T> httpResult) {

            if (!httpResult.success()) {
                throw new ServerException(httpResult.code,httpResult.message);
            }
            return httpResult.data==null? (T) "" :httpResult.data;
        }
    }

    public static class HttpResultFunc<T> implements Function<Throwable, Observable<T>> {
        @Override
        public Observable<T> apply(Throwable throwable) {
            //ExceptionEngine为处理异常的驱动器
            return Observable.error(ExceptionEngine.handleException(throwable));
        }
    }

}

