package com.running.penguin.api;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @Author : zlang-pc
 * @Createtime : 2020/6/2 11:02
 * @Describe :
 */
public interface ApiService {

    @GET("ad")
    Observable<ApiResult<String>> getSplash(@Query("positionCode") String positionCode);

}
