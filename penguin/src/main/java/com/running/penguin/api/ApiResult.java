package com.running.penguin.api;

/**
 * @Author : zlang-pc
 * @Createtime : 2020/6/2 10:33
 * @Describe :
 */
/**
*@Author : zlang
*@CreateTime : 2020/6/2 0002 13:14
*@Describe : 
*/
public class ApiResult<T> {

    public int code;
    public String message;
    public T data;

    public boolean success(){
        return code==0;
    }

    @Override
    public String toString() {
        return "ApiResult{" +
                "code=" + code +
                ", msg='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
