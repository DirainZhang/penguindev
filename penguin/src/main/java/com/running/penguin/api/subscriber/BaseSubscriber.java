package com.running.penguin.api.subscriber;

import com.running.penguin.api.exception.ApiException;

import io.reactivex.Observer;

/**
 * @Author : zlang-pc
 * @Createtime : 2020/6/2 10:41
 * @Describe :
 */
abstract class BaseSubscriber<T> implements Observer<T> {

    @Override
    public void onError(Throwable e) {
        if (e != null) {
            e.printStackTrace();
        }
        if(e instanceof ApiException){
            onError((ApiException)e);
        }else{
            onError(new ApiException(e,-1));
        }

        onComplete();
    }

    /***
     * 错误回调
     */
    protected abstract void onError(ApiException ex);

}
