package com.running.penguin.api.exception;

/**
 * @Author : zlang-pc
 * @Createtime : 2020/6/2 10:37
 * @Describe :
 */
public class Error {

    public static final int EMPTY = 0x1110;
    public static final int HTTP_ERROR = 0x1111;
    public static final int NETWORK_ERROR = 0x1112;
    public static final int SERVER_ERROR = 0x1113;
    public static final int UNKNOWN = 0x1114;
    public static final int LOGIN_OUT = 1008;
}
