package com.running.penguin.http.okhttp;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/***
 * 为每个请求添加固定的Header
 */
public class HeaderInterceptor implements Interceptor {

    String token = "eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFMyNTYifQ.eyJ1c2VySWQiOiI2IiwiZXhwIjoxNTk3NjQ4NzQxLCJpc3MiOiJ3c2xNb24gQXVnIDAzIDE1OjE5OjAxIENTVCAyMDIwIiwiYXVkIjoiaHR0cDovL3d3dy5ib3RvdmlzaW9uLmNvbSJ9.qOgrfMb00EOCQGy7l3SnOSuPYve8N9FtcRu_Svh9vyo";
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder builder = request.newBuilder();
        builder.addHeader("Access_User_Token", token);
//        new Retrofit.Builder()
//                .baseUrl(RetrofitManager.BASE_URL)
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create())
//                .addConverterFactory(ScalarsConverterFactory.create())
//                .build().create(ApiService.class).getToken("").subscribe(new SimpleSubscriber<ApiResult<String>>() {
//            @Override
//            public void onNext(ApiResult<String> stringApiResult) {
//                if(stringApiResult!=null&&stringApiResult.data!=null){
//                    android.util.Log.e("ysy","获取accessToken:"+stringApiResult.data);
//                    Data.getUser().setAccessToken(stringApiResult.data);
//                    Data.updateUser( Data.getUser(),false, Utils.getContext());
//                }
//            }
//
//            @Override
//            protected void onError(ApiException ex) {
//
//            }
//        });
        return chain.proceed(builder.build());
    }

}
