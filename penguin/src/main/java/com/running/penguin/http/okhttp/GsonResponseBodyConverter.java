package com.running.penguin.http.okhttp;


import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;

import java.io.IOException;

import io.reactivex.annotations.NonNull;
import okhttp3.ResponseBody;
import retrofit2.Converter;

public class GsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    private final Gson gson;
    private final TypeAdapter<T> adapter;

    GsonResponseBodyConverter(Gson gson, TypeAdapter<T> adapter) {
        this.gson = gson;
        this.adapter = adapter;
    }

    @SuppressWarnings({"unchecked", "SameParameterValue"})
    @Override
    public T convert(@NonNull ResponseBody value) throws IOException {
        java.lang.reflect.Type superclass = adapter.getClass().getGenericSuperclass();
        if (superclass instanceof java.lang.reflect.ParameterizedType) {
            java.lang.reflect.Type type = ((java.lang.reflect.ParameterizedType) superclass).getActualTypeArguments()[0];
            if (type instanceof Class && type == String.class) {
                return (T) value.string();
            }
        }
        JsonReader jsonReader = gson.newJsonReader(value.charStream());
        jsonReader.setLenient(true);
        try {
            return adapter.read(jsonReader);
        } finally {
            value.close();
        }
    }
}