
package com.running.penguin.integration;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import java.util.List;


/**
 * {@link Application.ActivityLifecycleCallbacks} 默认实现类
 */
public class ActivityLifecycle implements Application.ActivityLifecycleCallbacks {
    private com.running.penguin.integration.AppManager mAppManager;
    private List<FragmentManager.FragmentLifecycleCallbacks> mFragmentLifecycles;

    public ActivityLifecycle(@NonNull com.running.penguin.integration.AppManager appManager, @NonNull List<FragmentManager.FragmentLifecycleCallbacks> fragmentLifecycles) {
        mAppManager = appManager;
        mFragmentLifecycles = fragmentLifecycles;
    }

    @Override
    public void onActivityCreated(Activity activity, @NonNull Bundle savedInstanceState) {
        //如果 intent 包含了此字段,并且为 true 说明不加入到 list 进行统一管理
        boolean isNotAdd = false;
        if (activity.getIntent() != null) {
            isNotAdd = activity.getIntent().getBooleanExtra(com.running.penguin.integration.AppManager.IS_NOT_ADD_ACTIVITY_LIST, false);
        }


        if (!isNotAdd) {
            mAppManager.addActivity(activity);
        }

        registerFragmentCallbacks(activity);

    }


    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        mAppManager.setCurrentActivity(activity);

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        if (mAppManager.getCurrentActivity() == activity) {
            mAppManager.setCurrentActivity(null);
        }

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, @NonNull Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        mAppManager.removeActivity(activity);

    }


    /**
     * 给每个 Activity 的所有 Fragment 设置监听其生命周期
     *
     * @param activity
     */
    private void registerFragmentCallbacks(Activity activity) {

        //注册框架外部, 开发者扩展的 Fragment 生命周期逻辑
        for (FragmentManager.FragmentLifecycleCallbacks fragmentLifecycle : mFragmentLifecycles) {
            if (activity instanceof FragmentActivity) {
                ((FragmentActivity) activity).getSupportFragmentManager().registerFragmentLifecycleCallbacks(fragmentLifecycle, true);
            }
        }
    }


}
