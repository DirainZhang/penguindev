package com.running.penguin.mvp;

import androidx.annotation.NonNull;

/**
 * View的基类
 */

public interface IView {


    /**
     * 显示加载
     */
    void showLoading();

    /**
     * 隐藏加载
     */
    void hideLoading();

    /**
     * 显示信息
     *
     * @param message 消息内容, 不能为 {@code null}
     */
    void showMessage(@NonNull String message);

    /**
     * 显示无网络状态
     */
    void showNoNetState();

    void showContentState();

    void showEmptyState();

    void showErrorState();

    void refreshCompleted();

    void nonMoreData(boolean hasMore);
}
