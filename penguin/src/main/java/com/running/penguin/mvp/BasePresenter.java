/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.running.penguin.mvp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Presenter的基类，绑定View，注册和取消Rxjava，添加订阅
 * <p>
 * // * @param <V> MVP中的View
 */

public abstract class BasePresenter<V> {

    // 其实是不能为 null 的。这里加标识让 kt Presenter 中都判空避免异步时 View 被销毁后回调空指针
    @Nullable
    protected V mView = null;

    /**
     * RxJava 任务管理
     */
    public CompositeDisposable mCompositeDisposable;

    public BasePresenter(V view) {
        attachView(view);
    }


    /**
     * 初始化操作
     *
     * @param mvpView view
     */
    public void attachView(V mvpView) {
        this.mView = mvpView;
    }


    /**
     * 页面销毁时会调用
     */
    public void detachView() {
        unDispose();
        this.mView = null;
        this.mCompositeDisposable = null;
    }


    /**
     * 将 {@link Disposable} 添加到 {@link CompositeDisposable} 中统一管理
     * 可在 {@link AppCompatActivity#onDestroy()} 中使用 {@link #unDispose()} 停止正在执行的 RxJava 任务,避免内存泄漏(框架已自行处理)
     *
     * @param disposable Disposable
     */
    @SuppressWarnings("JavadocReference")
    public void addDispose(Disposable disposable) {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
        }
        mCompositeDisposable.add(disposable);//将所有 Disposable 放入集中处理
    }

    /**
     * 停止集合中正在执行的 RxJava 任务
     */
    public void unDispose() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();//保证 Activity 结束时取消所有正在执行的订阅
        }
    }

    /**
     * 获取 dispose
     *
     * @return
     */
    public CompositeDisposable getDispose() {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
        }
        return mCompositeDisposable;
    }
}