package com.running.penguin.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;

import com.flyco.animation.BaseAnimatorSet;
import com.flyco.dialog.widget.base.BottomTopBaseDialog;
import com.running.penguin.utils.ScreenUtils;

import java.util.Objects;

/**
 * @Author : zlang-pc
 * @Createtime : 2020/6/9 19:00
 * @Describe :
 */
public abstract class CenterBaseDialog<T extends CenterBaseDialog<T>> extends BottomTopBaseDialog<T> {
    private BaseAnimatorSet mWindowInAs;
    private BaseAnimatorSet mWindowOutAs;

    public CenterBaseDialog(Context context, View animateView) {
        super(context);
        this.mAnimateView = animateView;
        this.mInnerShowAnim = new TranslateAnimation(1, 0.0F, 1, 0.0F, 1, 1.0F, 1, 0.0F);
        this.mInnerDismissAnim = new TranslateAnimation(1, 0.0F, 1, 0.0F, 1, 0.0F, 1, 1.0F);
    }

    public CenterBaseDialog(Context context) {
        this(context, (View)null);
    }

    protected void onStart() {
        super.onStart();
        this.mLlTop.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.mLlTop.setGravity(Gravity.CENTER);
        this.getWindow().setGravity(Gravity.CENTER);
        this.mLlTop.setPadding(this.mLeft, this.mTop, this.mRight, this.mBottom);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.showWithAnim();
    }

    public void dismiss() {
        this.dismissWithAnim();
    }

    protected BaseAnimatorSet getWindowInAs() {
        if(this.mWindowInAs == null) {
            this.mWindowInAs = new CenterBaseDialog.WindowInAs();
        }

        return this.mWindowInAs;
    }

    protected BaseAnimatorSet getWindowOutAs() {
        if(this.mWindowOutAs == null) {
            this.mWindowOutAs = new CenterBaseDialog.WindowOutAs();
        }

        return this.mWindowOutAs;
    }

    private class WindowOutAs extends BaseAnimatorSet {
        private WindowOutAs() {
        }

        public void setAnimation(View view) {
            ObjectAnimator oa1 = ObjectAnimator.ofFloat(view, "scaleX", new float[]{0.9F, 1.0F});
            ObjectAnimator oa2 = ObjectAnimator.ofFloat(view, "scaleY", new float[]{0.9F, 1.0F});
            this.animatorSet.playTogether(new Animator[]{oa1, oa2});
        }
    }

    private class WindowInAs extends BaseAnimatorSet {
        private WindowInAs() {
        }

        public void setAnimation(View view) {
            ObjectAnimator oa1 = ObjectAnimator.ofFloat(view, "scaleX", new float[]{1.0F, 0.9F});
            ObjectAnimator oa2 = ObjectAnimator.ofFloat(view, "scaleY", new float[]{1.0F, 0.9F});
            this.animatorSet.playTogether(new Animator[]{oa1, oa2});
        }
    }

}
