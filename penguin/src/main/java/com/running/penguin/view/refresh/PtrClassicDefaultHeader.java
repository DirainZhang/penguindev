package com.running.penguin.view.refresh;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.running.penguin.R;

public class PtrClassicDefaultHeader extends FrameLayout implements PtrUIHandler {

    private ImageView mRotateView;
     private AnimationDrawable mRefreshingAd;

    public PtrClassicDefaultHeader(Context context) {
        super(context);
        initViews(null);
    }

    public PtrClassicDefaultHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(attrs);
    }

    public PtrClassicDefaultHeader(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs);
    }

    protected void initViews(AttributeSet attrs) {
         View header = LayoutInflater.from(getContext()).inflate(R.layout.cube_ptr_classic_default_header, this);
        mRotateView =  header.findViewById(R.id.refresh_img);
        mRefreshingAd = (AnimationDrawable) mRotateView.getDrawable();
        resetView();
    }



    private void resetView() {
        if (mRotateView != null) {
            mRefreshingAd.stop();
         }
     }



    @Override
    public void onUIReset(PtrFrameLayout frame) {
         resetView();
    }

    @Override
    public void onUIRefreshPrepare(PtrFrameLayout frame) {
         if (frame.isPullToRefresh()) {
            mRefreshingAd.start();
//             Glide.with(getContext()).load(R.mipmap.refresh_gif).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(mRotateView);
        } else {
//              Glide.with(getContext()).load(R.mipmap.refresh_gif).asBitmap().into(mRotateView);
            mRefreshingAd.stop();
        }
    }

    @Override
    public void onUIRefreshBegin(PtrFrameLayout frame) {
        mRefreshingAd.start();
//        Glide.with(getContext()).load(R.mipmap.refresh_gif).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(mRotateView);
    }

    @Override
    public void onUIRefreshComplete(PtrFrameLayout frame) {
         mRefreshingAd.stop();
//         Glide.with(getContext()).load(R.mipmap.refresh_gif).asBitmap().into(mRotateView);
    }


    @Override
    public void onUIPositionChange(PtrFrameLayout frame, boolean isUnderTouch, byte status, PtrIndicator ptrIndicator) {
        final int mOffsetToRefresh = frame.getOffsetToRefresh();
        final int currentPos = ptrIndicator.getCurrentPosY();
        final int lastPos = ptrIndicator.getLastPosY();
         if (currentPos < mOffsetToRefresh && lastPos >= mOffsetToRefresh) {
            if (isUnderTouch && status == PtrFrameLayout.PTR_STATUS_PREPARE) {
                crossRotateLineFromBottomUnderTouch(frame);
            }
        } else if (currentPos > mOffsetToRefresh && lastPos <= mOffsetToRefresh) {
            if (isUnderTouch && status == PtrFrameLayout.PTR_STATUS_PREPARE) {
                crossRotateLineFromTopUnderTouch(frame);
            }
        }
    }

    private void crossRotateLineFromTopUnderTouch(PtrFrameLayout frame) {
        if (!frame.isPullToRefresh()) {
            mRefreshingAd.start();
//            Glide.with(getContext()).load(R.mipmap.refresh_gif).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(mRotateView);

        }
    }

    private void crossRotateLineFromBottomUnderTouch(PtrFrameLayout frame) {
         if (frame.isPullToRefresh()) {
             mRefreshingAd.start();
//             Glide.with(getContext()).load(R.mipmap.refresh_gif).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(mRotateView);
        } else {
             mRefreshingAd.stop();
//              Glide.with(getContext()).load(R.mipmap.refresh_gif).asBitmap().into(mRotateView);
        }
    }


    public void destoryView() {
        if (mRotateView != null) {
            mRefreshingAd.stop();
            mRefreshingAd =null;
            removeView(mRotateView);
            mRotateView = null;
        }
    }
}
