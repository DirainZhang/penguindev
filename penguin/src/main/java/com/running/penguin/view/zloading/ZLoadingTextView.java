package com.running.penguin.view.zloading;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;

import androidx.annotation.NonNull;

import com.running.penguin.R;
import com.running.penguin.view.zloading.text.TextBuilder;

/**
 * @Author : zlang-pc
 * @Createtime : 2018/12/19 10:46
 * @Describe :
 */
public class ZLoadingTextView extends ZLoadingView {
    private String mText = "Zyao89";

    public ZLoadingTextView(Context context) {
        this(context, null);
    }

    public ZLoadingTextView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public ZLoadingTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @Override
    @Deprecated
    public void setLoadingBuilder(@NonNull Z_TYPE builder) {
        super.setLoadingBuilder(Z_TYPE.TEXT);
    }

    public void setText(String text) {
        this.mText = text;
        if (mZLoadingBuilder instanceof TextBuilder) {
            ((TextBuilder) mZLoadingBuilder).setText(mText);
        }
    }

    private void init(Context context, AttributeSet attrs) {
        super.setLoadingBuilder(Z_TYPE.TEXT);
        try {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ZLoadingTextView);
            String text = ta.getString(R.styleable.ZLoadingTextView_z_text);
            ta.recycle();
            if (!TextUtils.isEmpty(text)) {
                this.mText = text;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        setText(mText);
        super.onAttachedToWindow();
    }
}
