package com.running.penguin.view.zloading;

import com.running.penguin.view.zloading.ball.ElasticBallBuilder;
import com.running.penguin.view.zloading.ball.InfectionBallBuilder;
import com.running.penguin.view.zloading.ball.IntertwineBuilder;
import com.running.penguin.view.zloading.circle.DoubleCircleBuilder;
import com.running.penguin.view.zloading.circle.PacManBuilder;
import com.running.penguin.view.zloading.circle.RotateCircleBuilder;
import com.running.penguin.view.zloading.circle.SingleCircleBuilder;
import com.running.penguin.view.zloading.circle.SnakeCircleBuilder;
import com.running.penguin.view.zloading.clock.CircleBuilder;
import com.running.penguin.view.zloading.clock.ClockBuilder;
import com.running.penguin.view.zloading.path.MusicPathBuilder;
import com.running.penguin.view.zloading.path.SearchPathBuilder;
import com.running.penguin.view.zloading.path.StairsPathBuilder;
import com.running.penguin.view.zloading.rect.ChartRectBuilder;
import com.running.penguin.view.zloading.rect.StairsRectBuilder;
import com.running.penguin.view.zloading.star.LeafBuilder;
import com.running.penguin.view.zloading.star.StarBuilder;
import com.running.penguin.view.zloading.text.TextBuilder;

/**
*@Author : zlang-pc
*@Createtime : 2018/12/19 10:52
*@Describe :
*/
public enum Z_TYPE
{
    CIRCLE(CircleBuilder.class),
    CIRCLE_CLOCK(ClockBuilder.class),
    STAR_LOADING(StarBuilder.class),
    LEAF_ROTATE(LeafBuilder.class),
    DOUBLE_CIRCLE(DoubleCircleBuilder.class),
    PAC_MAN(PacManBuilder.class),
    ELASTIC_BALL(ElasticBallBuilder.class),
    INFECTION_BALL(InfectionBallBuilder.class),
    INTERTWINE(IntertwineBuilder.class),
    TEXT(TextBuilder.class),
    SEARCH_PATH(SearchPathBuilder.class),
    ROTATE_CIRCLE(RotateCircleBuilder.class),
    SINGLE_CIRCLE(SingleCircleBuilder.class),
    SNAKE_CIRCLE(SnakeCircleBuilder.class),
    STAIRS_PATH(StairsPathBuilder.class),
    MUSIC_PATH(MusicPathBuilder.class),
    STAIRS_RECT(StairsRectBuilder.class),
    CHART_RECT(ChartRectBuilder.class),;

    private final Class<?> mBuilderClass;

    Z_TYPE(Class<?> builderClass)
    {
        this.mBuilderClass = builderClass;
    }

    <T extends ZLoadingBuilder> T newInstance()
    {
        try
        {
            return (T) mBuilderClass.newInstance();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
