package com.running.penguin.view.refresh.api;

import android.content.Context;

import androidx.annotation.NonNull;

/**
*@Author : zlang-pc
*@Createtime : 2020/6/3 14:06
*@Describe : 默认Header创建器
*/
public interface DefaultRefreshHeaderCreator {
    @NonNull
    RefreshHeader createRefreshHeader(@NonNull Context context, @NonNull RefreshLayout layout);
}
