package com.running.penguin.view.refresh.header;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.running.penguin.R;
import com.running.penguin.view.refresh.api.RefreshHeader;
import com.running.penguin.view.refresh.api.RefreshKernel;
import com.running.penguin.view.refresh.api.RefreshLayout;
import com.running.penguin.view.refresh.constant.RefreshState;
import com.running.penguin.view.refresh.constant.SpinnerStyle;
import com.running.penguin.view.refresh.util.DensityUtil;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/***
 * 贝塞尔曲线类雷达风格刷新组件
 * Created by lcodecore on 2016/10/2.
 */

@SuppressWarnings({"UnusedReturnValue", "unused"})
public class ysyRefreshHeader extends FrameLayout implements RefreshHeader {
    private AnimationDrawable mDrawable;

    //<editor-fold desc="FrameLayout">
    public ysyRefreshHeader(Context context) {
        this(context, null);
    }

    public ysyRefreshHeader(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ysyRefreshHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, AttributeSet attrs, int defStyleAttr) {
        setMinimumHeight(DensityUtil.dp2px(80));
        ImageView imageView = new ImageView(getContext());
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setImageResource(R.drawable.refresh_layout);
        mDrawable = (AnimationDrawable) imageView.getDrawable();
        this.addView(imageView, MATCH_PARENT, MATCH_PARENT);
    }


    @Override
    @NonNull
    public View getView() {
        return this;
    }

    @NonNull
    @Override
    public SpinnerStyle getSpinnerStyle() {
        return SpinnerStyle.Translate;
    }

    @Override
    public void setPrimaryColors(int... colors) {

    }

    @Override
    public void onInitialized(@NonNull RefreshKernel kernel, int height, int extendHeight) {
    }

    @Override
    public void onMoving(boolean isDragging, float percent, int offset, int height, int maxDragHeight) {

    }

    @Override
    public boolean isSupportHorizontalDrag() {
        return false;
    }

    @Override
    public void onHorizontalDrag(float percentX, int offsetX, int offsetMax) {

    }


    @Override
    public void onReleased(final RefreshLayout layout, int height, int extendHeight) {

    }

    @Override
    public void onStartAnimator(@NonNull RefreshLayout layout, int height, int extendHeight) {

    }

    @Override
    public int onFinish(@NonNull RefreshLayout layout, boolean success) {
        return 400;
    }

    @Override
    public void onStateChanged(RefreshLayout refreshLayout, RefreshState oldState, RefreshState newState) {
        switch (newState) {
            case None:
            case RefreshFinish:
                if (mDrawable != null && mDrawable.isRunning()) {
                    mDrawable.stop();
                    mDrawable.selectDrawable(0);
                }
                break;
            case PullDownToRefresh:
                break;
            case PullUpToLoad:
                break;
            case Refreshing:
                if (mDrawable != null && !mDrawable.isRunning()) {
                    mDrawable.start();
                }
                break;
            case Loading:
                break;
        }
    }
}
