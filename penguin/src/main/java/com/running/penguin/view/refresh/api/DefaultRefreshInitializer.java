package com.running.penguin.view.refresh.api;

import android.content.Context;

import androidx.annotation.NonNull;

/**
*@Author : zlang-pc
*@Createtime : 2020/6/3 14:07
*@Describe : 默认全局初始化器
*/
public interface DefaultRefreshInitializer {
    void initialize(@NonNull Context context, @NonNull RefreshLayout layout);
}
