package com.running.penguin.view.refresh.listener;

import com.running.penguin.view.refresh.api.RefreshLayout;

/**
 * 加载更多监听器
 * Created by scwang on 2017/5/26.
 */
public interface OnLoadMoreListener {
    void onLoadMore(RefreshLayout refreshLayout);
}
