package com.running.penguin.view.refresh.footer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.ColorUtils;
import com.running.penguin.R;
import com.running.penguin.view.refresh.api.RefreshFooter;
import com.running.penguin.view.refresh.api.RefreshKernel;
import com.running.penguin.view.refresh.api.RefreshLayout;
import com.running.penguin.view.refresh.constant.RefreshState;
import com.running.penguin.view.refresh.constant.SpinnerStyle;
import com.running.penguin.view.refresh.footer.ballpulse.BallPulseView;
import com.running.penguin.view.refresh.util.DensityUtil;
import static android.view.View.MeasureSpec.AT_MOST;
import static android.view.View.MeasureSpec.getSize;
import static android.view.View.MeasureSpec.makeMeasureSpec;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/***
 * 球脉冲底部加载组件
 * Created by SCWANG on 2017/5/30.
 */
@SuppressWarnings({"unused", "UnusedReturnValue"})
public class HomeBallPulseFooter extends ViewGroup implements RefreshFooter {

    private BallPulseView mBallPulseView;
    private SpinnerStyle mSpinnerStyle = SpinnerStyle.Translate;
    private Integer mNormalColor;
    private Integer mAnimationColor;
    private View mInflate;
    private View mBottomTextLayout;

    //<editor-fold desc="ViewGroup">
    public HomeBallPulseFooter(@NonNull Context context) {
        super(context);
        initView(context, null, 0);
    }

    public HomeBallPulseFooter(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs, 0);
    }

    public HomeBallPulseFooter(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, AttributeSet attrs, int defStyleAttr) {
        mInflate = LayoutInflater.from(context).inflate(R.layout.home_refresh_bottom, null);
        mBallPulseView = mInflate.findViewById(R.id.bottom_bp);
        mBottomTextLayout = mInflate.findViewById(R.id.bottom_text_layout);
        addView(mInflate, WRAP_CONTENT, WRAP_CONTENT);
        setMinimumHeight(DensityUtil.dp2px(60));
     }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSpec = makeMeasureSpec(getSize(widthMeasureSpec), AT_MOST);
        int heightSpec = makeMeasureSpec(getSize(heightMeasureSpec), AT_MOST);
        mInflate.measure(widthSpec, heightSpec);
        setMeasuredDimension(
                resolveSize(mInflate.getMeasuredWidth(), widthMeasureSpec),
                resolveSize(mInflate.getMeasuredHeight(), heightMeasureSpec)
        );
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int p_width = getMeasuredWidth();
        int p_height = getMeasuredHeight();
        int c_width = mInflate.getMeasuredWidth();
        int c_height = mInflate.getMeasuredHeight();
        int left = p_width / 2 - c_width / 2;
        int top = p_height / 2 - c_height / 2;
        mInflate.layout(left, top, left + c_width, top + c_height);
    }
    //</editor-fold>

    //<editor-fold desc="RefreshFooter">
    @Override
    public void onInitialized(@NonNull RefreshKernel kernel, int height, int extendHeight) {

    }

    @Override
    public void onMoving(boolean isDragging, float percent, int offset, int height, int maxDragHeight) {

    }

    @Override
    public boolean isSupportHorizontalDrag() {
        return false;
    }

    @Override
    public void onHorizontalDrag(float percentX, int offsetX, int offsetMax) {
    }



    @Override
    public void onReleased(RefreshLayout layout, int footerHeight, int extendHeight) {

    }

    @Override
    public void onStartAnimator(@NonNull RefreshLayout layout, int footerHeight, int extendHeight) {
        mBallPulseView.startAnim();
    }

    @Override
    public void onStateChanged(RefreshLayout refreshLayout, RefreshState oldState, RefreshState newState) {

    }

    @Override
    public int onFinish(@NonNull RefreshLayout layout, boolean success) {
        mBallPulseView.stopAnim();
        return 0;
    }

    @Override
    public boolean setNoMoreData(boolean noMoreData) {
        if(noMoreData){
            mBottomTextLayout.setVisibility(VISIBLE);
        }else {
            mBottomTextLayout.setVisibility(GONE);
        }
        return true;
    }

    @Override@Deprecated
    public void setPrimaryColors(@ColorInt int... colors) {
        if (mAnimationColor == null && colors.length > 1) {
            mBallPulseView.setAnimatingColor(colors[0]);
        }
        if (mNormalColor == null) {
            if (colors.length > 1) {
                mBallPulseView.setNormalColor(colors[1]);
            } else if (colors.length > 0) {
                mBallPulseView.setNormalColor(ColorUtils.compositeColors(0x99ffffff,colors[0]));
            }
        }
    }
    @NonNull
    @Override
    public View getView() {
        return this;
    }

    @NonNull
    @Override
    public SpinnerStyle getSpinnerStyle() {
        return mSpinnerStyle;
    }
    //</editor-fold>

    //<editor-fold desc="API">
    public HomeBallPulseFooter setSpinnerStyle(SpinnerStyle mSpinnerStyle) {
        this.mSpinnerStyle = mSpinnerStyle;
        return this;
    }

    public HomeBallPulseFooter setIndicatorColor(@ColorInt int color) {
        mBallPulseView.setIndicatorColor(color);
        return this;
    }

    public HomeBallPulseFooter setNormalColor(@ColorInt int color) {
        mNormalColor = color;
        mBallPulseView.setNormalColor(color);
        return this;
    }

    public HomeBallPulseFooter setAnimatingColor(@ColorInt int color) {
        mAnimationColor = color;
        mBallPulseView.setAnimatingColor(color);
        return this;
    }

    //</editor-fold>
}
