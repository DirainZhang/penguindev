package com.running.penguin.view.refresh;

import android.content.Context;
import android.util.AttributeSet;

public class PtrClassicFrameLayout extends PtrFrameLayout {

    private PtrClassicDefaultHeader mPtrClassicHeader;
    private ILoadMoreViewFactory mLoadMoreViewFactory;

    public PtrClassicFrameLayout(Context context) {
        super(context);
        initViews();
    }

    public PtrClassicFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public PtrClassicFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews();
    }

    public void initViews() {
        mPtrClassicHeader = new PtrClassicDefaultHeader(getContext());
        setHeaderView(mPtrClassicHeader);
        addPtrUIHandler(mPtrClassicHeader);

        mLoadMoreViewFactory = new DefaultLoadMoreViewFooter();
        setFooterView(mLoadMoreViewFactory);
    }

    public PtrClassicDefaultHeader getHeader() {
        return mPtrClassicHeader;
    }


    public void onDsetory(){
        if(mPtrClassicHeader!=null) {
            mPtrClassicHeader.destoryView();
            mPtrClassicHeader = null;
        }
        if(mLoadMoreViewFactory!=null) {
            mLoadMoreViewFactory = null;
        }
    }

}


