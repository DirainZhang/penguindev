package com.running.penguin.view.refresh.listener;

public interface CoordinatorLayoutListener {
    void onCoordinatorUpdate(boolean enableRefresh, boolean enableLoadMore);
}