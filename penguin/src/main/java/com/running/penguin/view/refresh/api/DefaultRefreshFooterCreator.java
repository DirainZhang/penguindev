package com.running.penguin.view.refresh.api;

import android.content.Context;

import androidx.annotation.NonNull;

/**
*@Author : zlang-pc
*@Createtime : 2020/6/3 14:06
*@Describe : 默认Footer创建器
*/
public interface DefaultRefreshFooterCreator {
    @NonNull
    RefreshFooter createRefreshFooter(@NonNull Context context, @NonNull RefreshLayout layout);
}
