package com.running.penguin.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.running.penguin.R;

/**
*@Author : zlang-pc
*@Createtime : 2018/12/21 10:29
*@Describe :
*/
public class ChildModuleLayout extends LinearLayout {

    private ImageView leftImageView;
    private TextView leftTextView;
    private TextView rightTextView;
    private RelativeLayout titleLayout;
    private LinearLayout container;
    private View bottomLineView;
    private View parentLineView;

    public ChildModuleLayout(Context context){
        this(context, null);
    }

    public ChildModuleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater mInflater = LayoutInflater.from(context);
        View myView = mInflater.inflate(R.layout.public_child_module_layout, null);
        leftImageView = (ImageView) myView.findViewById(R.id.icon);
        leftTextView = (TextView) myView.findViewById(R.id.left_text);
        rightTextView = (TextView) myView.findViewById(R.id.right_text);
        titleLayout = (RelativeLayout) myView.findViewById(R.id.title_layout);
        container = (LinearLayout) myView.findViewById(R.id.container);
        bottomLineView = (View) findViewById(R.id.bottom_line);
        parentLineView = (View) findViewById(R.id.parent_line);
        addView(myView);
    }

    public ChildModuleLayout setLeftImageView(int resId){
        leftImageView.setImageResource(resId);
        return this;
    }

    public ChildModuleLayout setLeftText(String text){
        leftTextView.setText(text);
        return this;
    }

    public ChildModuleLayout setRightText(String text){
        rightTextView.setText(text);
        return this;
    }

    public ChildModuleLayout setRightVisible(int visible){
        rightTextView.setVisibility(visible);
        return this;
    }

    public ChildModuleLayout setLeftTextColor(int textColor){
        leftTextView.setTextColor(getResources().getColor(textColor));
        return this;
    }

    public ChildModuleLayout setRightTextSize(float textSize){
        rightTextView.setTextSize(textSize);
        return this;
    }

    public ChildModuleLayout setRightTextColor(int textColor){
        rightTextView.setTextColor(getResources().getColor(textColor));
        return this;
    }

    public ChildModuleLayout setRightImageView(int resId){
        Drawable drawable = getResources().getDrawable(resId);
        // 这一步必须要做,否则不会显示.
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        rightTextView.setCompoundDrawables(null,null,drawable,null);
        return this;
    }

    /**
     * 标题点击监听
     */
    public ChildModuleLayout setClickTitleListener(View.OnClickListener onClickListener){
        titleLayout.setOnClickListener(onClickListener);
        return this;
    }

    public LinearLayout getContainer() {
        return container;
    }

    public void setContainer(LinearLayout container) {
        this.container = container;
    }

    public ImageView getLeftImageView() {
        return leftImageView;
    }

    public void setLeftImageView(ImageView leftImageView) {
        this.leftImageView = leftImageView;
    }

    public View getBottomLineView() {
        return bottomLineView;
    }

    public void setBottomLineView(View bottomLineView) {
        this.bottomLineView = bottomLineView;
    }

    public int getBottomLineVisible(boolean isVisible){
        return isVisible ? View.VISIBLE : View.GONE;
    }

    public View getParentLineView(){
        return parentLineView;
    }

    public void setParentLineView(View parentLineView){
        this.parentLineView = parentLineView;
    }

    public int getParentLineViewVisible(boolean isVisible){
        return isVisible ? View.VISIBLE : View.GONE;
    }

    public TextView getLeftTextView() {
        return leftTextView;
    }

    public void setLeftTextView(TextView leftTextView) {
        this.leftTextView = leftTextView;
    }

    public TextView getRightTextView() {
        return rightTextView;
    }

    public void setRightTextView(TextView rightTextView) {
        this.rightTextView = rightTextView;
    }

    public RelativeLayout getTitleLayout() {
        return titleLayout;
    }

    public void setTitleLayout(RelativeLayout titleLayout) {
        this.titleLayout = titleLayout;
    }

}
