package com.running.penguin.permission;

import android.os.Build;
import androidx.annotation.NonNull;
import com.running.penguin.permission.install.InstallRequest;
import com.running.penguin.permission.install.NInstallRequest;
import com.running.penguin.permission.install.OInstallRequest;
import com.running.penguin.permission.runtime.LRequest;
import com.running.penguin.permission.runtime.MRequest;
import com.running.penguin.permission.runtime.Request;
import com.running.penguin.permission.source.Source;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by huxinyu on 2018/12/20.
 * Email : panda.h@foxmail.com
 * <p>
 * Description :
 */
public class RequestBuilder {

    private Source mSource;

    protected RequestBuilder source(Source source) {
        mSource = source;
        return this;
    }

    /**
     * request permission s
     *
     * @param permissions the permissions request
     * @return request
     */
    public Request runtime(String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return new MRequest(mSource).permission(permissions);
        } else {
            return new LRequest(mSource).permission(permissions);
        }
    }

    /**
     * request groups of permissions
     *
     * @param groups permission group
     * @return request
     */
    public Request runtime(String[]... groups) {
        List<String> permissionList = new ArrayList<>();
        for (String[] group : groups) {
            permissionList.addAll(Arrays.asList(group));
        }
        String[] permissions = permissionList.toArray(new String[0]);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return new MRequest(mSource).permission(permissions);
        } else {
            return new LRequest(mSource).permission(permissions);
        }
    }

    public InstallRequest install(@NonNull File apk) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return new OInstallRequest(mSource).file(apk);
        } else {
            return new NInstallRequest(mSource).file(apk);
        }
    }

    public InstallRequest install(@NonNull String apkPath) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return new OInstallRequest(mSource).file(apkPath);
        } else {
            return new NInstallRequest(mSource).file(apkPath);
        }
    }
}
