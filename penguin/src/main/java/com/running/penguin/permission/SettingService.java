
package com.running.penguin.permission;

/**
 *
 * desc :
 */
public interface SettingService {

    /**
     * Execute setting.
     */
    void execute();

    /**
     * Cancel the operation.
     */
    void cancel();
}
