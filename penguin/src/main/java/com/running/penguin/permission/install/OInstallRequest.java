package com.running.penguin.permission.install;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import com.running.penguin.permission.PermissionActivity;
import com.running.penguin.permission.RequestExecutor;
import com.running.penguin.permission.source.Source;

import java.io.File;

/**
 * Created by huxinyu on 2018/12/20.
 * Email : panda.h@foxmail.com
 * <p>
 * Description :request install permission for Android O+
 */
public class OInstallRequest extends InstallRequestImp implements RequestExecutor, PermissionActivity.RequestListener {

    public OInstallRequest(Source source) {
        super.source(source);
    }


    @Override
    final  public void execute() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionActivity.requestInstall(mSource.getContext(), this);
        }
    }

    @Override
    public void cancel() {

    }


    @Override
    final  public void onRequestCallback() {
        new Handler(Looper.getMainLooper()).postDelayed(this::dispatchCallback, 100);
    }

    private void dispatchCallback() {
        if (mSource.canRequestPackageInstalls()) {
            callbackSucceed();
            installExecute();
        } else {
            callbackFailed();
        }
    }

    @Override
    final  public void start() {
        if (mSource.canRequestPackageInstalls()) {
            callbackSucceed();
            installExecute();
        } else {
            showRationale(this);
        }
    }

    @NonNull
    @Override
    public InstallRequest file(File apk) {
        mFile = apk;
        return this;
    }

    @NonNull
    @Override
    public InstallRequest file(String path) {
        mFile = new File(path);
        return this;
    }
}
