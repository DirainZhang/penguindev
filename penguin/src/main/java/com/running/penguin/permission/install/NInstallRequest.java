package com.running.penguin.permission.install;

import androidx.annotation.NonNull;
import com.running.penguin.permission.source.Source;

import java.io.File;

/**
 * Created by huxinyu on 2018/12/20.
 * Email : panda.h@foxmail.com
 * <p>
 * Description :request install permission for Android O-
 */
public class NInstallRequest extends InstallRequestImp {

    public NInstallRequest(Source source) {
        super.source(source);
    }


    @Override
    public void start() {
        callbackSucceed();
        installExecute();
    }

    @NonNull
    @Override
    public InstallRequest file(File apk) {
        mFile = apk;
        return this;
    }

    @NonNull
    @Override
    public InstallRequest file(String path) {
        mFile = new File(path);
        return this;
    }
}
