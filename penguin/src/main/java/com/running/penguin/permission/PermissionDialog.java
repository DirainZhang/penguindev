package com.running.penguin.permission;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Window;

import com.running.penguin.R;

/**
 * Created by huxinyu on 2018/9/20.
 * Email : panda.h@foxmail.com
 * <p>
 * Description : 权限设置的弹窗移到权限库中
 */
public class PermissionDialog {
    /**
     * 显示设置Dialog
     *
     * @param context     上下文
     * @param title       标题
     * @param msg         提示信息
     * @param positiveStr 确定文字
     * @param negativeStr 取消文字
     * @param isFinish    是否结束当前页面
     * @return AlertDialog
     */
    public static AlertDialog showSetting(final AppCompatActivity context, CharSequence title,
                                          CharSequence msg, CharSequence positiveStr, CharSequence negativeStr, final boolean isFinish) {
        SettingService settingService = Permission.permissionSetting(context);
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(true)
                .setPositiveButton(positiveStr, (dialog, which) -> {
                    settingService.execute();
                })
                .setNegativeButton(negativeStr, (dialog, which) -> {
                    settingService.cancel();
                    if (isFinish) { //结束当前页面
                        context.finish();
                    }
                })
                .create();
        Window window = alertDialog.getWindow();
        if (window != null) {
            window.setWindowAnimations(R.style.permission_setting_dialog_anim);
        }
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, R.color.icon));
        return alertDialog;
    }
}
