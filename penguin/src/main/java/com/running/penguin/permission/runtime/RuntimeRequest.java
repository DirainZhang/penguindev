package com.running.penguin.permission.runtime;

import androidx.annotation.NonNull;

/**
 * Created by huxinyu on 2018/12/24.
 * Email : panda.h@foxmail.com
 * <p>
 * Description :
 */
abstract class RuntimeRequest implements Request {
    /**
     * One or more permissions.
     */
    @NonNull
    abstract Request permission(String... permissions);

    /**
     * One or more permission groups.
     */
    @NonNull
    abstract Request permission(String[]... groups);
}
