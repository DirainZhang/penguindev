package com.running.penguin.permission.install;

import androidx.annotation.NonNull;
import com.running.penguin.permission.Action;

import java.io.File;

/**
 * Created by huxinyu on 2018/12/20.
 * Email : panda.h@foxmail.com
 * <p>
 * Description :install request
 */
public interface InstallRequest {

    /**
     * Action to be taken when all permissions are granted.
     */
    @NonNull
    InstallRequest onGranted(Action<File> granted);

    /**
     * Action to be taken when all permissions are denied.
     */
    @NonNull
    InstallRequest onDenied(Action<File> denied);

    void start();
}
