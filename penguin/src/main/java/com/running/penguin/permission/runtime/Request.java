
package com.running.penguin.permission.runtime;

import androidx.annotation.NonNull;
import com.running.penguin.permission.Action;
import com.running.penguin.permission.Rationale;

import java.util.List;

/**
 *
 * desc :
 */
public interface Request {

    /**
     * Set request rationale.
     */
    @NonNull
    Request rationale(Rationale listener);

    /**
     * Action to be taken when all permissions are granted.
     */
    @NonNull
    Request onGranted(Action<List<String>> granted);

    /**
     * Action to be taken when all permissions are denied.
     */
    @NonNull
    Request onDenied(Action<List<String>> denied);

    /**
     * Request permission.
     */
    void start();

}