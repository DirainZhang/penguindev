
package com.running.penguin.permission;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.running.penguin.permission.setting.PermissionSetting;
import com.running.penguin.permission.source.ContextSource;
import com.running.penguin.permission.source.Source;
import com.running.penguin.utils.Utils;

import java.util.List;

/**
 *
 * desc :
 */
public class Permission {

    private static final RequestBuilder builder = new RequestBuilder();

    /**
     * Some privileges permanently disabled, may need to set up in the execute.
     *
     * @param activity          {@link Activity}.
     * @param deniedPermissions one or more permissions.
     * @return true, other wise is false.
     */
    public static boolean hasAlwaysDeniedPermission(
            @NonNull Activity activity,
            @NonNull List<String> deniedPermissions) {
        return hasAlwaysDeniedPermission(new ContextSource(activity.getApplicationContext())
                , deniedPermissions);
    }

    /**
     * Some privileges permanently disabled, may need to set up in the execute.
     *
     * @param fragment          {@link Fragment}.
     * @param deniedPermissions one or more permissions.
     * @return true, other wise is false.
     */
    public static boolean hasAlwaysDeniedPermission(
            @NonNull Fragment fragment,
            @NonNull List<String> deniedPermissions) {
        Activity activity = fragment.getActivity();
        Context context;
        if (activity == null) {
            context = Utils.getContext();
        }else{
            context = activity.getApplicationContext();
        }
        return hasAlwaysDeniedPermission(new ContextSource(context), deniedPermissions);
    }

    /**
     * Some privileges permanently disabled, may need to set up in the execute.
     *
     * @param fragment          {@link android.app.Fragment}.
     * @param deniedPermissions one or more permissions.
     * @return true, other wise is false.
     */
    public static boolean hasAlwaysDeniedPermission(
            @NonNull android.app.Fragment fragment,
            @NonNull List<String> deniedPermissions) {
        Activity activity = fragment.getActivity();
        Context context;
        if (activity == null) {
            context = Utils.getContext();
        }else{
            context = activity.getApplicationContext();
        }
        return hasAlwaysDeniedPermission(new ContextSource(context), deniedPermissions);
    }

    /**
     * Some privileges permanently disabled, may need to set up in the execute.
     *
     * @param context           {@link Context}.
     * @param deniedPermissions one or more permissions.
     * @return true, other wise is false.
     */
    public static boolean hasAlwaysDeniedPermission(
            @NonNull Context context,
            @NonNull List<String> deniedPermissions) {
        return hasAlwaysDeniedPermission(new ContextSource(context), deniedPermissions);
    }

    /**
     * Has always been denied permission.
     */
    private static boolean hasAlwaysDeniedPermission(
            @NonNull Source source,
            @NonNull List<String> deniedPermissions) {
        for (String permission : deniedPermissions) {
            if (!source.isShowRationalePermission(permission)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Some privileges permanently disabled, may need to set up in the execute.
     *
     * @param activity          {@link Activity}.
     * @param deniedPermissions one or more permissions.
     * @return true, other wise is false.
     */
    public static boolean hasAlwaysDeniedPermission(
            @NonNull Activity activity,
            @NonNull String... deniedPermissions) {
        return hasAlwaysDeniedPermission(new ContextSource(activity.getApplicationContext())
                , deniedPermissions);
    }

    /**
     * Some privileges permanently disabled, may need to set up in the execute.
     *
     * @param fragment          {@link Fragment}.
     * @param deniedPermissions one or more permissions.
     * @return true, other wise is false.
     */
    public static boolean hasAlwaysDeniedPermission(
            @NonNull Fragment fragment,
            @NonNull String... deniedPermissions) {
        Activity activity = fragment.getActivity();
        Context context;
        if (activity == null) {
            context = Utils.getContext();
        }else{
            context = activity.getApplicationContext();
        }
        return hasAlwaysDeniedPermission(new ContextSource(context), deniedPermissions);
    }

    /**
     * Some privileges permanently disabled, may need to set up in the execute.
     *
     * @param fragment          {@link android.app.Fragment}.
     * @param deniedPermissions one or more permissions.
     * @return true, other wise is false.
     */
    public static boolean hasAlwaysDeniedPermission(
            @NonNull android.app.Fragment fragment,
            @NonNull String... deniedPermissions) {
        Activity activity = fragment.getActivity();
        Context context;
        if (activity == null) {
            context = Utils.getContext();
        }else{
            context = activity.getApplicationContext();
        }
        return hasAlwaysDeniedPermission(new ContextSource(context), deniedPermissions);
    }

    /**
     * Some privileges permanently disabled, may need to set up in the execute.
     *
     * @param context           {@link Context}.
     * @param deniedPermissions one or more permissions.
     * @return true, other wise is false.
     */
    public static boolean hasAlwaysDeniedPermission(
            @NonNull Context context,
            @NonNull String... deniedPermissions) {
        return hasAlwaysDeniedPermission(new ContextSource(context), deniedPermissions);
    }

    /**
     * Has always been denied permission.
     */
    private static boolean hasAlwaysDeniedPermission(
            @NonNull Source source,
            @NonNull String... deniedPermissions) {
        for (String permission : deniedPermissions) {
            if (!source.isShowRationalePermission(permission)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Create a service that opens the permission setting page.
     *
     * @param activity {@link Activity}.
     * @return {@link SettingService}.
     */
    @NonNull
    public static SettingService permissionSetting(@NonNull Activity activity) {
        return new PermissionSetting(new ContextSource(activity.getApplicationContext()));
    }

    /**
     * Create a service that opens the permission setting page.
     *
     * @param fragment {@link Fragment}.
     * @return {@link SettingService}.
     */
    @NonNull
    public static SettingService permissionSetting(@NonNull Fragment fragment) {
        Activity activity = fragment.getActivity();
        Context context;
        if (activity == null) {
            context = Utils.getContext();
        }else{
            context = activity.getApplicationContext();
        }
        return new PermissionSetting(new ContextSource(context));
    }

    /**
     * Create a service that opens the permission setting page.
     *
     * @param fragment {@link android.app.Fragment}.
     * @return {@link SettingService}.
     */
    @NonNull
    public static SettingService permissionSetting(@NonNull android.app.Fragment fragment) {
        Activity activity = fragment.getActivity();
        Context context;
        if (activity == null) {
            context = Utils.getContext();
        }else{
            context = activity.getApplicationContext();
        }
        return new PermissionSetting(new ContextSource(context));
    }

    /**
     * Create a service that opens the permission setting page.
     *
     * @param context {@link android.app.Fragment}.
     * @return {@link SettingService}.
     */
    @NonNull
    public static SettingService permissionSetting(@NonNull Context context) {
        return new PermissionSetting(new ContextSource(context));
    }

    /**
     * With Activity.
     *
     * @param activity {@link Activity}.
     * @return {@link com.running.penguin.permission.runtime.Request}.
     */
    @NonNull
    public static RequestBuilder with(@NonNull Activity activity) {
        return builder.source(new ContextSource(activity.getApplicationContext()));
    }

    /**
     * With android.support.v4.app.Fragment.
     *
     * @param fragment {@link Fragment}.
     * @return {@link com.running.penguin.permission.runtime.Request}.
     */
    @NonNull
    public static RequestBuilder with(@NonNull Fragment fragment) {
        Activity activity = fragment.getActivity();
        Context context;
        if (activity == null) {
            context = Utils.getContext();
        }else{
            context = activity.getApplicationContext();
        }
        return builder.source(new ContextSource(context));
    }

    /**
     * With android.app.Fragment.
     *
     * @param fragment {@link android.app.Fragment}.
     * @return {@link com.running.penguin.permission.runtime.Request}.
     */
    @NonNull
    public static RequestBuilder with(@NonNull android.app.Fragment fragment) {
        Activity activity = fragment.getActivity();
        Context context;
        if (activity == null) {
            context = Utils.getContext();
        }else{
            context = activity.getApplicationContext();
        }
        return builder.source(new ContextSource(context));
    }

    /**
     * With context.
     *
     * @param context {@link Context}.
     * @return {@link com.running.penguin.permission.runtime.Request}.
     */
    @NonNull
    public static RequestBuilder with(@NonNull Context context) {
        return builder.source(new ContextSource(context));
    }

    private Permission() {
    }

}