
package com.running.penguin.permission;

import android.content.Context;

import java.util.List;

/**
 *
 * desc :
 */
public interface Rationale {

    /**
     * Show rationale of permissions to user.
     *
     * @param context     context.
     * @param permissions show rationale permissions.
     * @param executor    executor.
     */
    void showRationale(Context context, List<String> permissions, RequestExecutor executor);
}