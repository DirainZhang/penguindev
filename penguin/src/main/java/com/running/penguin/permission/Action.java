
package com.running.penguin.permission;

/**
 *
 * desc :
 */
public interface Action<T> {

    /**
     * An action.
     *
     * @param permissions the current action under permissions.
     */
    void onAction(T permissions);

}
