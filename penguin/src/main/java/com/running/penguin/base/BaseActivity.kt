package com.running.penguin.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.running.penguin.base.delegate.IActivity
import com.running.penguin.constants.EConstants
import com.running.penguin.mvp.BasePresenter
import com.running.penguin.utils.NetWorkUtils
import com.running.penguin.utils.system.AppUtils
import com.running.penguin.utils.ui.ProgressDialogUtils
import com.running.penguin.view.StateLayout
import com.running.penguin.view.zloading.ZLoadingDialog
import java.lang.reflect.Field

/**
 *@Author : DirainZhang
 *@Createtime : 2020/9/15 14:56
 *@Describe : Activity基类
 */
abstract class BaseActivity<P : BasePresenter<*>> : AppCompatActivity(), IActivity,View.OnClickListener {

    protected var mPresenter: P? = null
    protected var mContext : Context? = null
    protected var mActivity : AppCompatActivity? = null
    protected var loading : ZLoadingDialog? = null
    protected var mStateLayout: StateLayout? = null
    companion object {
        const val STATE_REFRESH_MANUAL = 1
        const val STATE_REFRESH_NET_CHANGE = 2
    }
    private val mNetWorkStatusReceiver: NetWorkStatusReceiver by lazy {
        NetWorkStatusReceiver()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = applicationContext
        mActivity = this
        mPresenter = createPresenter()
        initVariables()
        try {
            val layoutResID = initLayout()
            //如果initView返回0,框架则不会调用setContentView(),当然也不会 Bind ButterKnife
            if (layoutResID != 0) {
                setContentView(layoutResID)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        initToolBar()
        getScreenSize()
        loading = AppUtils.LoadingDialog(mActivity,0,null)
        initViews(savedInstanceState)
        loadData(savedInstanceState)
    }

    private fun getScreenSize() {
        val outMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(outMetrics)
        EConstants.SCREEN_WIDTH = outMetrics.widthPixels.toDouble()
        EConstants.SCREEN_HEIGHT = outMetrics.heightPixels.toDouble()
    }

    protected abstract fun createPresenter(): P

    protected abstract fun initToolBar()

    /***
     * 初始化状态布局layout
     * @param stateLayoutId： StateLayout的id
     */
    fun initStateLayout(stateLayoutId: Int) {
        initStateLayout(findViewById<View>(stateLayoutId) as StateLayout)
    }

    /***
     * 初始化状态布局layout
     * @param stateLayout： StateLayout
     */
    fun initStateLayout(stateLayout: StateLayout?) {
        if (stateLayout == null) {
            return
        }
        this.mStateLayout = stateLayout
        mStateLayout?.setErrorClick { refreshDataWhenError(STATE_REFRESH_MANUAL) }
        mStateLayout?.setEmptyClick { refreshDataWhenError(STATE_REFRESH_MANUAL) }
    }

    /***
     * 当错误页面点击刷新或网络连接后自动刷新时调用
     * @param state
     * [.STATE_REFRESH_MANUAL] 错误页面手动点击刷新
     * [.STATE_REFRESH_NET_CHANGE] 网络连接后自动刷新
     */
    open fun refreshDataWhenError(state: Int) {
        showLoadingState()
    }

    /**
     * 刷新完成
     */
    protected open fun refreshCompleted() {}

    /**
     * 有无更多数据
     * true：有
     * false：无
     */
    protected open fun nonMoreData(hasMore: Boolean) {}

    /**
     * 显示无数据状态
     */
    protected open fun showEmptyState() {
        if (mStateLayout != null) {
            mStateLayout!!.showEmptyView()
        }
    }

    /**
     * 显示错误状态
     */
    protected open fun showErrorState() {
        if (mStateLayout != null) {
            mStateLayout!!.showErrorView()
        }
    }

    /**
     * 显示正在加载状态
     */
    protected open fun showLoadingState() {
        if (mStateLayout != null) {
            mStateLayout!!.showLoadingView()
        }
    }

    /***
     * 显示无网络状态
     */
    open fun showNoNetState() {
        if (mStateLayout != null) {
            mStateLayout?.showNoNetView()
            registerReceiver(mNetWorkStatusReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        }
    }

    /**
     * 显示内容，即正常页面
     */
    protected open fun showContentState() {
        if (mStateLayout != null) {
            mStateLayout!!.showContentView()
        }
    }

    inner class NetWorkStatusReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action == ConnectivityManager.CONNECTIVITY_ACTION) {
                val isConnected = NetWorkUtils.isNetworkConnected(context)
                if (isConnected && mStateLayout?.showState == 3) {
                    refreshDataWhenError(STATE_REFRESH_NET_CHANGE)
                }
            }
        }
    }

    override fun onDestroy() {
        fixInputMethodManagerLeak()
        super.onDestroy()
        mPresenter?.detachView()
        this.mPresenter = null
        mStateLayout = null
    }

    /**
     * 处理输入框引起的内存泄漏问题
     */
    private fun fixInputMethodManagerLeak() {
        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        val arr = arrayOf("mCurRootView", "mServedView", "mNextServedView")
        var f: Field
        var obj: Any?
        for (param in arr) {
            try {
                f = imm.javaClass.getDeclaredField(param)
                if (!f.isAccessible) {
                    f.isAccessible = true
                } // author: sodino mail:sodino@qq.com
                obj = f.get(imm)
                if (obj != null && obj is View) {
                    val view = obj as View?
                    if (view?.context === this) { // 被InputMethodManager持有引用的context是想要目标销毁的
                        f.set(imm, null) // 置空，破坏掉path to gc节点
                    } else {
                        // 不是想要目标销毁的，即为又进了另一层界面了，不要处理，避免影响原逻辑,也就不用继续for循环了
                        break
                    }
                }
            } catch (t: Throwable) {
                t.printStackTrace()
            }

        }
    }


    protected  open fun showLoading() {
//        ProgressDialogUtils.show(this, false)
        loading?.show()
    }

    protected open fun showLoading(msg: String) {
        ProgressDialogUtils.show(this, msg, false)
    }

    protected open fun hideLoading() {
//        ProgressDialogUtils.releaseDialog(this)
        loading?.dismiss()
    }

    override fun hideTitle(): Boolean {
        return false
    }

    override fun onClick(v: View?) {

    }

}
