package com.running.penguin.base;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

import com.alibaba.android.arouter.launcher.ARouter;
import com.running.penguin.base.delegate.AppDelegate;
import com.running.penguin.base.delegate.AppLifecycles;
import com.running.penguin.integration.AppManager;
import com.running.penguin.utils.Utils;

import timber.log.Timber;


/**
*@Author : zlang
*@CreateTime : 2020/6/2 0002 15:10
*@Describe :
*/
public class BaseApplication extends Application {
    private AppLifecycles mAppDelegate;
    private static Application global;
    private boolean isDebugARouter = true;

    public static Context getGlobalContext() {
        return global.getApplicationContext();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        Timber.d("APP_START_APPLICATION %s", String.valueOf(System.currentTimeMillis()));
        MultiDex.install(this);
        if (mAppDelegate == null)
            this.mAppDelegate = new AppDelegate(base);
        this.mAppDelegate.attachBaseContext(base);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        global = this;
        Utils.init(this);
        AppManager.init(this);
        if (mAppDelegate != null) {
            this.mAppDelegate.onCreate(this);
        }
        if (isDebugARouter){
            ARouter.openLog();
            ARouter.openDebug();
        }
        ARouter.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        if (mAppDelegate != null) {
            this.mAppDelegate.onTerminate(this);
        }
        ARouter.getInstance().destroy();
    }

}
