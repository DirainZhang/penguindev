package com.running.penguin.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.running.penguin.base.delegate.IFragment;
import com.running.penguin.mvp.BasePresenter;
import com.running.penguin.utils.NetWorkUtils;
import com.running.penguin.utils.system.AppUtils;
import com.running.penguin.view.StateLayout;
import com.running.penguin.view.zloading.ZLoadingDialog;

import timber.log.Timber;


/**
 * <br>
 * Desc:Fragment基类
 * <br>
 */
public abstract class BaseFragment<P extends BasePresenter> extends Fragment implements IFragment {

    public static final int STATE_REFRESH_MANUAL = 1;
    public static final int STATE_REFRESH_NET_CHANGE = 2;

    protected StateLayout mStateLayout;

    protected NetWorkStatusReceiver mNetWorkStatusReceiver;

    protected P mPresenter;
    /**
     * 依附的Activity
     */
    protected AppCompatActivity mActivity;

    protected Context mContext;
    /**
     * fragment的view
     */
    protected View mView;
    /**
     * 是否已经初始化过，如果页面只刷新一次则在数据刷新完后设置为true，如果每次都需要刷新则设置为false
     */
    protected boolean mIsInited;
    /**
     * view是否绑定完成
     */
    protected boolean mIsPrepared;

    protected ZLoadingDialog loading;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity) context;
        mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loading = AppUtils.LoadingDialog(mActivity,0,null);
        initViews(view);
        loadData(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initVariables();
        mPresenter = createPresenter();
        try {
            int layoutResID = initContentView();
            //如果initView返回0,框架则不会调用setContentView(),当然也不会 Bind ButterKnife
            if (layoutResID != 0) {
                mView = inflater.inflate(layoutResID, null, false);

            }
        } catch (Exception e) {
            Timber.e(e);
        }
        processMatter();
        mIsPrepared = true;
        return mView;
    }

    /**
     * 初始化状态布局layout
     *
     * @param stateLayout： StateLayout
     */
    protected void initStateLayout(StateLayout stateLayout) {
        if (stateLayout == null) return;
        this.mStateLayout = stateLayout;
        mStateLayout.setErrorClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshDataWhenError(STATE_REFRESH_MANUAL);
            }
        });
    }

    /**
     * 初始化状态布局layout
     *
     * @param view：          fragment inflate view
     * @param stateLayoutId： StateLayout的id
     */
    protected void initStateLayout(View view, int stateLayoutId) {
        if (view == null) return;
        this.initStateLayout((StateLayout) view.findViewById(stateLayoutId));
    }

    /**
     * 当错误页面点击刷新或网络连接后自动刷新时调用
     *
     * @param state {@link #STATE_REFRESH_MANUAL} 错误页面手动点击刷新
     *              {@link #STATE_REFRESH_NET_CHANGE} 网络连接后自动刷新
     */
    protected void refreshDataWhenError(int state) {

    }

    /**
     * fragment是否显示出来,包括viewpager左右滑动、跳转到其他页面返回，从桌面返回。只要是让它显示出来就会调用
     *
     * @param isVisible    是否显示
     * @param isSwitchShow true 是从其他fragment切换后显示的 false为其他情况
     */
    protected void onFragmentVisibleChange(boolean isVisible, boolean isSwitchShow) {

    }

    /**
     * fragment一次显示，可以用于加载只加载一次的数据,
     * 注意：重复加载的请求不要放在这里，需要放置在onFragmentVisibleChange内
     */
    protected abstract void onFragmentFirstVisible();

    /**
     * 刷新完成
     */
    public void refreshCompleted() {

    }

    /**
     * 有无更多数据
     * true：有
     * false：无
     */
    public void nonMoreData(boolean hasMore) {

    }

    /**
     * 显示无数据状态
     */
    public void showEmptyState() {
        if (mStateLayout != null) {
            mStateLayout.showEmptyView();
        }
    }

    /**
     * 显示错误状态
     */
    public void showErrorState() {
        if (mStateLayout != null) {
            mStateLayout.showErrorView();
        }
    }

    /**
     * 显示正在加载状态
     */
    public void showLoadingState() {
        if (mStateLayout != null) {
            mStateLayout.showLoadingView();
        }
    }

    /**
     * 显示无网络状态
     */
    public void showNoNetState() {
        if (mStateLayout != null) {
            mStateLayout.showNoNetView();

            mNetWorkStatusReceiver = new NetWorkStatusReceiver();
            getContext().registerReceiver(mNetWorkStatusReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    /**
     * 显示内容，即正常页面
     */
    public void showContentState() {
        if (mStateLayout != null) {
            mStateLayout.showContentView();
        }
    }

    public class NetWorkStatusReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                boolean isConnected = NetWorkUtils.isNetworkConnected(getContext());
                if (isConnected && !isHidden()) {
                    refreshDataWhenError(STATE_REFRESH_NET_CHANGE);
                }
            }
        }
    }

    protected abstract P createPresenter();


    public void lazyLoad() {
        if (getUserVisibleHint() && mIsPrepared && !mIsInited) {
            onFragmentFirstVisible();
            mIsInited = true;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            lazyLoad();
        }
        if (mIsPrepared) {
            onFragmentVisibleChange(isVisibleToUser, true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()) {
            onFragmentVisibleChange(true, false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIsPrepared = false;
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        mStateLayout = null;
    }

    public void showLoading() {
//        ProgressDialogUtils.show(mActivity, false);
        loading.show();
    }

    public void showLoading(String msg) {
//        ProgressDialogUtils.show(mActivity, msg, false);
        loading.show();
    }

    public void hideLoading() {
//        ProgressDialogUtils.releaseDialog(mActivity);
        loading.dismiss();
    }

    public void processMatter(){

    }
}
