
package com.running.penguin.base.delegate;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import com.running.penguin.integration.ActivityLifecycle;
import com.running.penguin.integration.AppManager;
import com.running.penguin.integration.ConfigModule;
import com.running.penguin.integration.ManifestParser;

import java.util.ArrayList;
import java.util.List;

/**
 * desc :AppDelegate 可以代理 Application 的生命周期,在对应的生命周期,执行对应的逻辑
 */
public class AppDelegate implements com.running.penguin.base.delegate.AppLifecycles {
    private Application mApplication;
    private Application.ActivityLifecycleCallbacks mActivityLifecycle;
    private List<ConfigModule> mModules;
    private List<com.running.penguin.base.delegate.AppLifecycles> mAppLifecycles = new ArrayList<>();
    private List<Application.ActivityLifecycleCallbacks> mActivityLifecycles = new ArrayList<>();
    private List<FragmentManager.FragmentLifecycleCallbacks> mFragmentLifecycles = new ArrayList<>();

    public AppDelegate(@NonNull Context context) {

        //用反射, 将 AndroidManifest.xml 中带有 ConfigModule 标签的 class 转成对象集合（List<ConfigModule>）
        this.mModules = new ManifestParser(context).parse();

        //遍历之前获得的集合, 执行每一个 ConfigModule 实现类的某些方法
        for (ConfigModule module : mModules) {
            //将框架外部, 开发者实现的 Application 的生命周期回调 (AppLifecycles) 存入 mAppLifecycles 集合 (此时还未注册回调)
            module.injectAppLifecycle(context, mAppLifecycles);

            //将框架外部, 开发者实现的 Activity 的生命周期回调 (ActivityLifecycleCallbacks) 存入 mActivityLifecycles 集合 (此时还未注册回调)
            module.injectActivityLifecycle(context, mActivityLifecycles);

            //将框架外部, 开发者实现的 Fragment 的生命周期回调 (FragmentLifecycleCallbacks) 存入 mFragmentLifecycles 集合 (此时还未注册回调)
            module.injectFragmentLifecycle(context, mFragmentLifecycles);
        }
    }

    @Override
    public void attachBaseContext(@NonNull Context base) {
        //遍历 mAppLifecycles, 执行所有已注册的 AppLifecycles 的 attachBaseContext() 方法 (框架外部, 开发者扩展的逻辑)
        for (com.running.penguin.base.delegate.AppLifecycles lifecycle : mAppLifecycles) {
            lifecycle.attachBaseContext(base);
        }
    }

    @Override
    public void onCreate(@NonNull Application application) {
        this.mApplication = application;
        // 初始化 AppManager
        AppManager.init(application);

        for (ConfigModule module : mModules) {
            module.applyOptions(application);
        }
        this.mModules = null;
        mActivityLifecycle = new ActivityLifecycle(AppManager.getInstance(), mFragmentLifecycles);
        //注册框架内部已实现的 Activity 生命周期逻辑
        mApplication.registerActivityLifecycleCallbacks(mActivityLifecycle);

        //注册框架外部, 开发者扩展的 Activity 生命周期逻辑
        //每个 ConfigModule 的实现类可以声明多个 Activity 的生命周期回调
        //也可以有 N 个 ConfigModule 的实现类 (完美支持组件化项目 各个 Module 的各种独特需求)
        for (Application.ActivityLifecycleCallbacks lifecycle : mActivityLifecycles) {
            mApplication.registerActivityLifecycleCallbacks(lifecycle);
        }

        //执行框架外部, 开发者扩展的 App onCreate 逻辑
        for (com.running.penguin.base.delegate.AppLifecycles lifecycle : mAppLifecycles) {
            lifecycle.onCreate(mApplication);
        }

    }

    @Override
    public void onTerminate(@NonNull Application application) {
        if (mActivityLifecycle != null) {
            AppManager.getInstance().release();
            mApplication.unregisterActivityLifecycleCallbacks(mActivityLifecycle);
        }
        if (mActivityLifecycles != null && !mActivityLifecycles.isEmpty()) {
            for (Application.ActivityLifecycleCallbacks lifecycle : mActivityLifecycles) {
                mApplication.unregisterActivityLifecycleCallbacks(lifecycle);
            }
        }
        if (mAppLifecycles != null && !mAppLifecycles.isEmpty()) {
            for (com.running.penguin.base.delegate.AppLifecycles lifecycle : mAppLifecycles) {
                lifecycle.onTerminate(mApplication);
            }
        }
        this.mActivityLifecycle = null;
        this.mActivityLifecycles = null;
        this.mAppLifecycles = null;
        this.mApplication = null;
    }


}

