package com.running.penguin.base.delegate;

import android.os.Bundle;
import androidx.annotation.Nullable;

/**
 * desc :Activity 执行接口
 */
public interface IActivity {

    /**
     * 初始化变量,包括Intent数据
     */
    void initVariables();

    /**
     * 设置layout资源
     *
     * @return 资源文件id
     */
    int initLayout();

    /**
     * 初始化View和绑定事件
     */
    void initViews(@Nullable Bundle savedInstanceState);

    /**
     * 调用数据
     */
    void loadData(@Nullable Bundle savedInstanceState);

    /**
     *
     * @return true 隐藏
     */
    boolean hideTitle();
}
