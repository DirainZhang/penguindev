package com.running.penguin.base.delegate;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

/**
 * desc : Fragment 执行接口
 */
public interface IFragment {

    /**
     * 初始化变量,包括Intent数据
     */
    void initVariables();

    /**
     * 设置layout资源
     *
     * @return 资源文件id
     */
    int initContentView();

    /**
     * 初始化View和绑定事件
     */
    void initViews(View view);

    /**
     * 调用数据
     */
    void loadData(@Nullable Bundle savedInstanceState);
}
